# pull official base image
FROM python:3.10.7-alpine
# set work directory
WORKDIR /usr/src/kiddisvit
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev mc
RUN echo "testOK"
# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .

RUN python manage.py migrate
RUN python manage.py create_admin
