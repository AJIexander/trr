from django.contrib import admin
from django.contrib.admin import display

from .models import *


class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'email', 'phone_number', 'is_children', 'parent']


class ChildrenAdmin(admin.ModelAdmin):
    list_display = ['id', 'get_username', 'get_email', 'age', 'gender', 'get_all_score', 'get_parent', 'profile_images']

    @display(description='Нікнейм')
    def get_username(self, obj):
        return obj.children_user.username

    @display(description='Пошта')
    def get_email(self, obj):
        return obj.children_user.email

    @display(description='Кількість балів')
    def get_all_score(self, obj):
        return obj.children_score.total_score

    @display(description='Батьки')
    def get_parent(self, obj):
        return obj.children_user.parent


class AllScoreAdmin(admin.ModelAdmin):
    list_display = ['id', 'kid', 'get_email', 'total_score']

    @display(description='Пошта')
    def get_email(self, obj):
        return obj.kid.children_user.email


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'get_username', 'client', 'get_phone_number', 'country', 'city', 'zip_code', 'age', 'profile_images']
    list_display_links = ['get_username']

    @display(description='Нікнейм')
    def get_username(self, obj):
        return obj.client.username

    @display(description='Номер мобільного телефону')
    def get_phone_number(self, obj):
        return obj.client.phone_number


class CurrencyWalletAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'currency']


class FriendsAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'friend', 'confirmed', 'user_confirmed_children', 'friend_confirmed_children']


admin.site.register(User, UserAdmin)
admin.site.register(ChildrenProfile, ChildrenAdmin)
admin.site.register(AllScore, AllScoreAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(CurrencyWallet, CurrencyWalletAdmin)
admin.site.register(Friends, FriendsAdmin)
