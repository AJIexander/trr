import os

from django.core.management import BaseCommand

from accounts.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        new_admin = User.objects.filter(email=os.environ.get("DJANGO_SUPERUSER_EMAIL", 'admin@admin.com')).first()
        if new_admin:
            print('Administrator account is already registered')
        else:
            admin = User.objects.create_superuser(
                email=os.environ.get("DJANGO_SUPERUSER_EMAIL", 'admin@admin.com'),
                username=os.environ.get("DJANGO_SUPERUSER_USERNAME", "admin")
            )
            admin.set_password(os.environ.get("DJANGO_SUPERUSER_PASSWORD", 'admin'))
            admin.is_active = True
            admin.save(update_fields=['password', 'is_active'])
