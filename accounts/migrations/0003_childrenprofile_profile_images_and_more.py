# Generated by Django 4.1.1 on 2023-01-26 14:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_alter_allscore_options_alter_childrenprofile_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='childrenprofile',
            name='profile_images',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='accounts/profile_images/'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='profile_images',
            field=models.ImageField(blank=True, default=None, null=True, upload_to='accounts/profile_images/'),
        ),
    ]
