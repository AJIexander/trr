from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    class Meta:
        db_table = 'accounts_users'
        verbose_name_plural = 'Юзери'
    is_children = models.BooleanField(null=True, verbose_name='Является дитиною')
    parent = models.ForeignKey('self', on_delete=models.CASCADE, related_name='child', null=True, default=None,
                               verbose_name='Батьки')
    username = models.CharField(max_length=30, unique=False, verbose_name='Нікнейм')
    email = models.EmailField(max_length=50, unique=True, verbose_name='Пошта')
    phone_number = models.CharField(max_length=13, unique=True, default=None, null=True, blank=True,
                                     verbose_name='Номер мобільного телефону')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def check_balance(self, child_id: int):
        balance = CurrencyWallet.objects.filter(user__id=child_id).first()
        if balance.currency > 0:
            return balance.currency
        else:
            return None


class ChildrenProfile(models.Model):
    class Meta:
        db_table = 'accounts_children'
        verbose_name_plural = 'Облікові записи дітей'

    GENDERS = {'male', 'female', 'unknown'}

    age = models.IntegerField(default=None, null=True, verbose_name='Вік')
    gender = models.CharField(max_length=10, default=None, null=True, verbose_name='Стать')
    children_user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='children_profile',
                                         verbose_name='Дитина')
    profile_images = models.ImageField(null=True, default=None, blank=True, upload_to='accounts/profile_images/')

    def assign_to_course(self, course: object):
        self.course_rels.create(course=course)
        for task in course.started_tasks.all():
            self.children_task_progress_rels.create(task_relation=task)

    def __str__(self):
        return self.children_user.username

    def __repr__(self):
        return self.children_user.username


class AllScore(models.Model):
    class Meta:
        db_table = 'accounts_children_scores'
        verbose_name_plural = 'Рахунки дітей'

    kid = models.OneToOneField(ChildrenProfile, on_delete=models.CASCADE, related_name='children_score',
                                         verbose_name='Дитина')
    total_score = models.IntegerField(default=0, verbose_name='Кількість балів')

    def add_score(self, score: int) -> None:
        self.total_score += score
        self.save(update_fields=['total_score'])

    def __str__(self):
        return self.kid.children_user.username


class UserProfile(models.Model):
    class Meta:
        db_table = 'accounts_user_profile'
        verbose_name_plural = 'Облікові записи юзерів'
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='clients', verbose_name='Юзер')
    country = models.CharField(max_length=50, verbose_name='Країна', null=True, default=None, blank=True)
    city = models.CharField(max_length=50, verbose_name='Місто', null=True, default=None, blank=True)
    zip_code = models.CharField(max_length=50, verbose_name='поштовий індекс', null=True, default=None, blank=True)
    age = models.IntegerField(verbose_name='Вік', null=True, default=None, blank=True)
    profile_images = models.ImageField(null=True, default=None, blank=True, upload_to='accounts/profile_images/')
    prime_account = models.BooleanField(default=False)

    def __str__(self):
        return self.client.username


class ResetPasswordCode(models.Model):
    class Meta:
        db_table = 'accounts_reset_password_code'
        verbose_name_plural = 'Коди для сбросу пароля'
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='code', verbose_name='Юзер')
    code = models.CharField(max_length=4, verbose_name='Код')
    create_ad = models.DateTimeField(null=True, auto_now_add=True, verbose_name='Час генерації')


class CurrencyWallet(models.Model):
    class Meta:
        verbose_name_plural = 'Гаманець для валюти'
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='wallet', verbose_name='Кліент')
    currency = models.IntegerField(default=0, verbose_name='Баланс гаманця')


class Friends(models.Model):
    class Meta:
        verbose_name_plural = 'Друзі'
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='client', verbose_name='Кліент')
    friend = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend_user', verbose_name='Друзі')
    confirmed = models.BooleanField(default=False, verbose_name='Підтверджено')
    user_confirmed_children = models.BooleanField(default=False)
    friend_confirmed_children = models.BooleanField(default=False)

    def __str(self):
        return self.friend.username