from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from accounts.models import User, ResetPasswordCode, UserProfile


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    phone_number = serializers.CharField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])

    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'phone_number']
        extra_kwargs = {
            'username': {'required': True},
            'email': {'required': True},
            'phone_number': {'required': True},
        }

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            phone_number=validated_data['phone_number']
        )
        user.set_password(validated_data['password'])
        user_profile = UserProfile.objects.create(client=user)
        user_profile.save()
        user.save()
        return user


class CreateTokenSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(CreateTokenSerializer, cls).get_token(user)
        token['email'] = user.email
        return token


class RegisterChildrenSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(required=True)
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    age = serializers.IntegerField(required=False)
    gender = serializers.CharField(required=False)


class GetStatisticSerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)


class EditChildrenProfileSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    username = serializers.CharField(required=False)
    age = serializers.IntegerField(required=False)
    gender = serializers.CharField(required=False)


class ChangePasswordSerializer(serializers.Serializer):
    model = User

    old_password = serializers.CharField(required=True, validators=[validate_password])
    new_password = serializers.CharField(required=True, validators=[validate_password])


class ChangePasswordChildrenSerializer(serializers.Serializer):
    model = User

    id = serializers.IntegerField(required=False)
    old_password = serializers.CharField(required=True, validators=[validate_password])
    new_password = serializers.CharField(required=True, validators=[validate_password])


class ResetPasswordSerializer(serializers.Serializer):
    model = User

    email = serializers.EmailField(required=True)


class CheckCodeSerializers(serializers.Serializer):
    model = ResetPasswordCode

    code = serializers.IntegerField(required=True)
    new_password = serializers.CharField(required=True, validators=[validate_password])


class CheckClientIdSerializer(serializers.Serializer):
    client_id = serializers.IntegerField(required=True)


class EditUserInfoSerializers(serializers.Serializer):
    model = UserProfile

    username = serializers.CharField(required=False)
    country = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    zip_code = serializers.IntegerField(required=False)
    phone_number = serializers.CharField(required=False)
    age = serializers.IntegerField(required=False)


class AddPhotoSerializers(serializers.Serializer):
    photo = serializers.FileField(required=True)
    children_id = serializers.IntegerField(required=False)


class TransferCurrencySerializers(serializers.Serializer):
    currency = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)


class TransferCurrencyChildrenSerializers(serializers.Serializer):
    currency = serializers.IntegerField(required=True)
    user_id = serializers.IntegerField(required=True)
    recipient_id = serializers.IntegerField(required=True)


class ChangeParentUserSerializer(serializers.Serializer):
    user_id = serializers.IntegerField(required=True)
    children_id = serializers.IntegerField(required=True)


class CheckPhoneUserSerializer(serializers.Serializer):
    phone_number = serializers.CharField(required=True, max_length=13)
