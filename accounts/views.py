import datetime
from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView
from Kiddisvit.settings import ADMIN_EMAIL, DOMAIN
from accounts.models import User, ChildrenProfile, AllScore, ResetPasswordCode, UserProfile, CurrencyWallet, Friends
from accounts.serializers import RegisterSerializer, CreateTokenSerializer, \
    GetStatisticSerializer, RegisterChildrenSerializer, EditChildrenProfileSerializer, ChangePasswordSerializer, \
    ChangePasswordChildrenSerializer, ResetPasswordSerializer, CheckCodeSerializers, CheckClientIdSerializer, \
    EditUserInfoSerializers, AddPhotoSerializers, TransferCurrencySerializers, \
    TransferCurrencyChildrenSerializers, ChangeParentUserSerializer, CheckPhoneUserSerializer
import random
from datetime import timedelta
from django.template.loader import get_template
from django.core.mail import EmailMessage

from assignments.models import MethodologiesCategory


# Create your views here.
class RegisterView(generics.CreateAPIView):
    """
    Documentation endpoint
    Register User
    """
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class TokenCreate(TokenObtainPairView):
    """
    Documentation endpoint
    Token create
    """
    permission_classes = (AllowAny,)
    serializer_class = CreateTokenSerializer


class RegisterChildren(APIView):
    """
    Documentation endpoint
    Register Children
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=RegisterChildrenSerializer)
    def post(self, request):
        serializer = RegisterChildrenSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children_user = User.objects.create(
            username=serializer.validated_data['username'],
            email=serializer.validated_data['email'],
            is_children=True,
            parent_id=request.user.pk
        )
        children_user.set_password(serializer.validated_data['password'])
        children_user.save()
        children = ChildrenProfile.objects.create(
            age=serializer.validated_data.get('age') or None,
            gender=serializer.validated_data.get('gender') or None,
            children_user=children_user)
        AllScore.objects.create(kid=children)
        CurrencyWallet.objects.create(user=children_user)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Child added'}}, status=201)


class GetChildrenInfo(APIView):
    """
    Documentation endpoint
    Getting children's points associated with the account from which the request for this endpoint comes
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        queryset = ChildrenProfile.objects.filter(
            Q(children_user_id=request.user)
            | Q(children_user__parent=request.user)
        ).first()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        children = AllScore.objects.filter(
            Q(kid__children_user__parent=queryset.children_user.parent_id)
            | Q(kid__children_user=queryset.children_user)
        ).all()
        stats = []
        for child in children:
            stats.append({
                'id': child.id,
                'name': child.kid.children_user.username,
                'age': child.kid.age,
                'gender': child.kid.gender,
                'score': child.total_score,
                'photo': f'{DOMAIN}{child.kid.profile_images.url}' if child.kid.profile_images else None,
                'wallet_currency': [wallet.currency for wallet in child.kid.children_user.wallet.all()],
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': stats
        }, status=200)


class EditChildrenProfile(APIView):
    """
    Edit children profile and edit children name
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=EditChildrenProfileSerializer)
    def post(self, request):
        serializer = EditChildrenProfileSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children_profile = ChildrenProfile.objects.filter(
            id=serializer.validated_data['id'],
            children_user__parent=request.user
        ).first()
        if not children_profile:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        children_profile.children_user.username = serializer.validated_data.get('username') \
                                                  or children_profile.children_user.username
        children_profile.children_user.save()
        children_profile.age = serializer.validated_data.get('age') or children_profile.age
        children_profile.gender = serializer.validated_data.get('gender') or children_profile.gender
        children_profile.save()
        data = {
            'name': children_profile.children_user.username,
            'age': children_profile.age,
            'gender': children_profile.gender
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class ChangePasswordUser(APIView):
    """
    An endpoint for changing password user.
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ChangePasswordSerializer)
    def post(self, request):
        serializer = ChangePasswordSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(id=request.user.pk).first()
        if not user.check_password(serializer.data.get("old_password")):
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': {"old_password": ["Wrong password."]}}}, status=400)

        user.set_password(serializer.data.get("new_password"))
        user.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Password updated successfully'}}, status=201)


class ChangePasswordChildren(APIView):
    """
    An endpoint for changing password children.
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ChangePasswordChildrenSerializer)
    def post(self, request):
        serializer = ChangePasswordChildrenSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = ChildrenProfile.objects.filter(
            Q(children_user_id=request.user) |
            Q(children_user__parent=request.user)
        ).first()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        children = User.objects.filter(
            Q(id=queryset.children_user.id) |
            Q(id=serializer.validated_data.get('id'))
        ).first()

        if not children.check_password(serializer.validated_data['old_password']):
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': {"old_password": ["Wrong password."]}}}, status=400)
        children.set_password(serializer.validated_data['new_password'])
        children.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Password updated successfully'}}, status=201)



class UserResetPassword(APIView):
    """
    Reset password
    send user email
    """

    @swagger_auto_schema(request_body=ResetPasswordSerializer)
    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(email=serializer.validated_data['email']).first()
        if not user:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Email not found'}}, status=404)

        generate_code = random.randint(1000, 9999)
        code = ResetPasswordCode.objects.create(user=user, code=generate_code)
        ctx = {
            'user': user.username,
            'email': user.email,
            'code': code.code
        }
        message = get_template('send_code_reset_password.html').render(ctx)
        msg = EmailMessage(
            'Subject',
            message,
            f'{ADMIN_EMAIL}',
            [f'{user.email}'],
        )
        msg.content_subtype = "html"
        msg.send()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Check you box'}}, status=201)


class CheckCodeResetPassword(APIView):
    """
    Check code and change password
    """
    @swagger_auto_schema(request_body=CheckCodeSerializers)
    def post(self, request):
        serializer = CheckCodeSerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        code = ResetPasswordCode.objects.filter(code=serializer.validated_data['code']).first()
        if not code:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'No such code was found'}}, status=404)

        if not code.create_ad.replace(tzinfo=None) + timedelta(seconds=300) > datetime.datetime.utcnow():
            code.delete()
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'Token usage time has expired'}}, status=400)

        code.user.set_password(serializer.validated_data['new_password'])
        code.user.save()
        code.delete()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Done! You change password'}}, status=201)


class GetInfoProgressMethodologies(APIView):
    """
    Making progress on methodologies and types of intelligence
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetStatisticSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        child_stat = []
        for meth in MethodologiesCategory.objects.all():
            subs = []
            for sub_category in meth.methodologies_rel.all():
                subs.append({
                    "name": sub_category.name,
                    "score": sum([curs.get_score_for_child(child_id=serializer.validated_data['children_id'])
                                  for curs in sub_category.course_rels.all()])
                })
            child_stat.append({
                'methodology_id': meth.id,
                'methodology_name': meth.name,
                'methodology_score': sum([sub['score'] for sub in subs]),
                'sub_categories': subs,
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': child_stat
        }, status=200)


class GetInfoUserProfile(APIView):
    """
    get info user profile
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, client_id: int):
        serializer = CheckClientIdSerializer(data={'client_id': client_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(id=serializer.validated_data['client_id']).first()
        profile_info = UserProfile.objects.filter(client=user).first()
        if not profile_info:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Юзера не існує'}}, status=404)

        data = [{
            'id': profile_info.id,
            'username': profile_info.client.username,
            'email': profile_info.client.email,
            'country': profile_info.country if profile_info.country else None,
            'city': profile_info.city if profile_info.city else None,
            'zip_code': profile_info.zip_code if profile_info.zip_code else None,
            'phone_number': profile_info.client.phone_number,
            'age': profile_info.age if profile_info.age else None,
            'photo': f'{DOMAIN}{profile_info.profile_images.url}' if profile_info.profile_images else None,
        }]
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class EditInfoUserProfile(APIView):
    """
    edit user profile info
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=EditUserInfoSerializers)
    def post(self, request):
        serializer = EditUserInfoSerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        info_profile = UserProfile.objects.filter(client=request.user).first()
        info_profile.client.username = serializer.validated_data.get('username') or info_profile.client.username
        info_profile.country = serializer.validated_data.get('country') or info_profile.country
        info_profile.city = serializer.validated_data.get('city') or info_profile.city
        info_profile.zip_code = serializer.validated_data.get('zip_code') or info_profile.zip_code
        info_profile.client.phone_number = serializer.validated_data.get('phone_number') \
                                           or info_profile.client.phone_number
        info_profile.age = serializer.validated_data.get('age') or info_profile.age
        info_profile.save()
        data = {
            'username': info_profile.client.username,
            'country': info_profile.country,
            'city': info_profile.city,
            'zip_code': info_profile.zip_code,
            'phone_number': info_profile.client.phone_number,
            'age': info_profile.age
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetUserId(APIView):
    """
    Search users
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, client_id: int):
        serializer = CheckClientIdSerializer(data={'client_id': client_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        users = User.objects.filter(id=serializer.validated_data['client_id'], parent_id=None).first()
        if not users:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Інформація ще не додана'}}, status=404)

        data = {
            'id': users.id,
            'username': users.username,
            'email': users.email,
            'phone_number': users.phone_number,
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetMyInfoUserProfile(APIView):
    """
    get info user profile
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        profile_info = UserProfile.objects.filter(client=request.user).first()
        data = {
                'id': profile_info.client.id,
                'username': profile_info.client.username,
                'email': profile_info.client.email,
                'country': profile_info.country if profile_info.country else None,
                'city': profile_info.city if profile_info.city else None,
                'zip_code': profile_info.zip_code if profile_info.zip_code else None,
                'phone_number': profile_info.client.phone_number if profile_info.client.phone_number else None,
                'age': profile_info.age if profile_info.age else None,
                'photo': f'{DOMAIN}{profile_info.profile_images.url}' if profile_info.profile_images else None,
                'prime_account': profile_info.prime_account,
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class AddPhotoProfile(APIView):
    """
    Add photo for profile
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=AddPhotoSerializers)
    def post(self, request):
        serializer = AddPhotoSerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        if serializer.validated_data.get('children_id') is None:
            avatar = UserProfile.objects.filter(client=request.user).first()
            avatar.profile_images = serializer.validated_data['photo']
            avatar.save()
            return Response({
                'status': 'Success',
                'code': 201,
                'data': f'{DOMAIN}{avatar.profile_images.url}' if avatar.profile_images else None
            }, status=201)

        elif serializer.validated_data.get('children_id') is not None:
            avatar = ChildrenProfile.objects.filter(id=serializer.validated_data.get('children_id'),
                                                    children_user__parent=request.user.pk).first()
            avatar.profile_images = serializer.validated_data['photo']
            avatar.save()
            return Response({
                'status': 'Success',
                'code': 201,
                'data': f'{DOMAIN}{avatar.profile_images.url}' if avatar.profile_images else None
            }, status=201)

        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': 'Not valid id'
                }
            }, status=404)


class GetOneChildrenInfo(APIView):
    """
    Documentation endpoint
    Obtain the child points associated with the account from which this endpoint was requested via id
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetStatisticSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children = AllScore.objects.filter(kid_id=serializer.validated_data['children_id']).first()
        if not children:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'Not valid id'}}, status=400)

        data = {
            'id': children.id,
            'name': children.kid.children_user.username,
            'age': children.kid.age,
            'gender': children.kid.gender,
            'score': children.total_score,
            'photo': f'{DOMAIN}{children.kid.profile_images.url}' if children.kid.profile_images else None,
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetMyWallet(APIView):
    """
    get user wallet
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        wallet, _ = CurrencyWallet.objects.get_or_create(user=request.user)
        data = {
            'balance_currency': wallet.currency
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class TransferCurrency(APIView):
    """
    Transfer for wallet
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=TransferCurrencySerializers)
    def post(self, request):
        serializer = TransferCurrencySerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        wallet = CurrencyWallet.objects.filter(user=request.user).first()
        if not wallet.currency >= serializer.validated_data['currency']:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description':
                        'There are not enough funds in your account or the wallet has not yet been created'
                }
            }, status=400)

        transfer, _ = CurrencyWallet.objects.get_or_create(user_id=serializer.validated_data['user_id'])
        transfer.currency += serializer.validated_data['currency']
        transfer.save()
        wallet.currency -= serializer.validated_data['currency']
        wallet.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {
                'description': 'Done!'
            }
        }, status=201)


class GetMyChildrenWallet(APIView):
    """
    get children wallet
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetStatisticSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children = ChildrenProfile.objects.filter(
            id=serializer.validated_data['children_id'], children_user__parent=request.user.pk).first()
        if children:
            wallet, _ = CurrencyWallet.objects.get_or_create(user=children.children_user)
            data = {
                'balance_currency': wallet.currency,
                'user_id': wallet.user_id
            }
            return Response({
                'status': 'Success',
                'code': 200,
                'data': data
            }, status=200)

        else:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description': 'You are not a parent'
                }
            }, status=400)


class TransferCurrencyChildrenWallet(APIView):
    """
    Transfer currency in children account
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=TransferCurrencyChildrenSerializers)
    def post(self, request):
        serializer = TransferCurrencyChildrenSerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        wallet = CurrencyWallet.objects.filter(user_id=serializer.validated_data['user_id'],
                                               user__parent=request.user).first()
        if not wallet:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description': 'You are not a parent'
                }
            }, status=400)

        if not wallet.currency >= serializer.validated_data['currency']:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description':
                        'There are not enough funds in your account or the wallet has not yet been created'
                }
            }, status=400)

        transfer, _ = CurrencyWallet.objects.get_or_create(
            user_id=serializer.validated_data['recipient_id'])
        transfer.currency += serializer.validated_data['currency']
        transfer.save()
        wallet.currency -= serializer.validated_data['currency']
        wallet.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {
                'description': 'Done!'
            }
            }, status=201)


class TransferCurrencyChildrenWalletForParent(APIView):
    """
    Transfer currency in children account for parent
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=TransferCurrencySerializers)
    def post(self, request):
        serializer = TransferCurrencySerializers(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        wallet = CurrencyWallet.objects.filter(user_id=serializer.validated_data['user_id'],
                                               user__parent=request.user).first()
        if wallet:
            if wallet.currency >= serializer.validated_data['currency']:
                transfer = CurrencyWallet.objects.filter(user_id=request.user.pk).first()
                if transfer:
                    transfer.currency += serializer.validated_data['currency']
                    transfer.save()
                else:
                    CurrencyWallet.objects.create(user_id=request.user.pk,
                                                  currency=serializer.validated_data['currency'])
                wallet.currency -= serializer.validated_data['currency']
                wallet.save()
                return Response({
                    'status': 'Success',
                    'code': 201,
                    'data': {
                        'description': 'Done!'
                    }
                }, status=201)
            else:
                return Response({
                    'status': 'Error',
                    'code': 400,
                    'data': {
                        'description':
                            'There are not enough funds in your account or the wallet has not yet been created'
                    }
                }, status=400)
        else:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description': 'You are not a parent'
                }
            }, status=400)


class ChangeParent(APIView):
    """
    Change parent user
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ChangeParentUserSerializer)
    def post(self, request):
        serializer = ChangeParentUserSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children = ChildrenProfile.objects.filter(id=serializer.validated_data['children_id'],
                                                  children_user__parent=request.user.pk).first()
        if children:
            user = User.objects.filter(id=serializer.validated_data['user_id'], is_children=None).first()
            if user:
                children.children_user.parent = user
                children.children_user.save()
                return Response({
                    'status': 'Success',
                    'code': 201,
                    'data': {
                        'description': 'Done!'
                    }
                }, status=201)
            else:
                return Response({
                    'status': 'Error',
                    'code': 400,
                    'data': {
                        'description': 'Not user'
                    }
                }, status=400)
        else:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {
                    'description': 'You are not children'
                }
            }, status=400)


class SearchFriend(APIView):
    """
    Search users phone number
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, phone_number: str):
        serializer = CheckPhoneUserSerializer(data={'phone_number': phone_number})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        user = User.objects.filter(phone_number=serializer.validated_data['phone_number']).first()
        if user:
            data = {
                'id': user.id,
                'username': user.username
            }
            return Response({
                'status': 'Success',
                'code': 200,
                'data': data
            }, status=200)
        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': 'Not found'
                }
            }, status=404)


class AddFriends(APIView):
    """
    Add friends
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=CheckClientIdSerializer)
    def post(self, request):
        serializer = CheckClientIdSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        user = User.objects.filter(id=serializer.validated_data['client_id']).first()
        if user:
            add_friends = Friends.objects.filter(user=request.user, friend=user).first()
            if add_friends:
                return Response({
                    'status': 'Error',
                    'code': 400,
                    'data': {
                        'description': 'The request has already been sent'
                    }
                }, status=400)
            else:
                Friends.objects.create(user=request.user, friend=user)
                return Response({
                    'status': 'Success',
                    'code': 201,
                    'data': {
                        'description': 'Done!'
                    }
                }, status=201)
        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': 'User not found'
                }
            }, status=404)


class ConfirmedFriendRequest(APIView):
    """
    Confirmed Friend Request
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=CheckClientIdSerializer)
    def post(self, request):
        serializer = CheckClientIdSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        confirmed_request = Friends.objects.filter(user_id=serializer.validated_data['client_id'],
                                                   friend=request.user).first()
        if not confirmed_request:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': 'No request'
                }
            }, status=404)
        confirmed_request.confirmed = True
        confirmed_request.save()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)


class GetMyFriends(APIView):
    """
    Get my friends
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        friends = Friends.objects.filter(Q(user=request.user) | Q(friend=request.user), confirmed=True).all()
        if friends:
            data = []
            for friend in friends:
                if friend.user_id == request.user.pk:
                    user_confirmed = friend.user_confirmed_children
                    friend_confirmed = friend.friend_confirmed_children
                    friend = friend.friend
                else:
                    user_confirmed = friend.friend_confirmed_children
                    friend_confirmed = friend.user_confirmed_children
                    friend = friend.user
                data.append({
                    'id': friend.id,
                    'username': friend.username,
                    'user_confirmed_children': user_confirmed,
                    'friend_confirmed_children': friend_confirmed
                })
            return Response({
                'status': 'Success',
                'code': 200,
                'data': data
            }, status=200)
        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': "you don't have any friends yet"
                }
            }, status=404)


class CheckMyFriendsRequest(APIView):
    """
    Checking for a request to add as a friend
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        friends = Friends.objects.filter(friend=request.user, confirmed=False).all()
        if friends:
            data = []
            for friend in friends:
                data.append({
                    'id': friend.user.id,
                    'username': friend.user.username
                })
            return Response({
                'status': 'Success',
                'code': 200,
                'data': data
            }, status=200)
        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {
                    'description': "you don't have any request friends yet"
                }
            }, status=404)


class FriendsGetChildrenInfo(APIView):
    """
    Viewing the user's children
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, client_id: int):
        serializer = CheckClientIdSerializer(data={'client_id': client_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        friend_obj = Friends.objects.filter(Q(user_id=serializer.validated_data['client_id'], friend=request.user)
                                         | Q(user=request.user, friend_id=serializer.validated_data['client_id']),
                                         confirmed=True).first()
        if not friend_obj:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not right'}}, status=401)
        user = None
        if friend_obj.user_id == client_id and friend_obj.user_confirmed_children:
            user = friend_obj.user
        elif friend_obj.friend_id == client_id and friend_obj.friend_confirmed_children:
            user = friend_obj.friend

        queryset = ChildrenProfile.objects.filter(children_user__parent=user).first()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not confirmed view children'}}, status=401)

        children = AllScore.objects.filter(kid__children_user__parent=queryset.children_user.parent_id).all()
        stats = []
        for child in children:
            stats.append({
                'id': child.id,
                'name': child.kid.children_user.username,
                'age': child.kid.age,
                'gender': child.kid.gender,
                'score': child.total_score,
                'photo': f'{DOMAIN}{child.kid.profile_images.url}' if child.kid.profile_images else None,
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': stats
        }, status=200)


class DeleteAccounts(APIView):
    """
    Delete accounts
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def delete(self, request):
        User.objects.filter(id=request.user.pk).delete()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)


class DeleteChildrenAccounts(APIView):
    """
    Delete children accounts
    """
    @swagger_auto_schema()
    def delete(self, request, children_id: int):
        serializer = GetStatisticSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        children_prof = ChildrenProfile.objects.filter(id=serializer.validated_data['children_id'],
                                       children_user__parent_id=request.user).first()
        if children_prof:
            if request.GET.get('accept'):
                User.objects.filter(id=children_prof.children_user.id).delete()
                return Response({
                    'status': 'Success',
                    'code': 202,
                    'data': {
                        'description': 'Done!'
                    }
                }, status=202)
            elif children_prof.children_user.check_balance(children_prof.children_user.id) is not None:
                balance = children_prof.children_user.check_balance(children_prof.children_user.id)
                return Response({
                    'status': 'Success',
                    'code': 200,
                    'data': {
                        'description': f'{balance}!'
                    }
                })
            else:
                User.objects.filter(id=children_prof.children_user.id).delete()
                return Response({
                    'status': 'Success',
                    'code': 200,
                    'data': {
                        'description': 'Done!'
                    }
                }, status=200)
        else:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not children'}}, status=404)


class DeleteFriends(APIView):
    """
    Delete friends relations
    """
    @swagger_auto_schema()
    def delete(self, request, client_id: int):
        serializer = CheckClientIdSerializer(data={'client_id': client_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        Friends.objects.filter(Q(user=request.user, friend__id=serializer.validated_data['client_id'])
                               | Q(friend=request.user, user__id=serializer.validated_data['client_id'])).delete()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)


class FriendsConfirmedChildren(APIView):
    """
    Give friends access to view information about your children
    """

    @swagger_auto_schema(request_body=CheckClientIdSerializer)
    def post(self, request):
        serializer = CheckClientIdSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)
        confirmed = Friends.objects.filter(Q(user__id=serializer.validated_data['client_id'], friend=request.user)
                                           | Q(user=request.user, friend__id=serializer.validated_data['client_id']),
                                           confirmed=True).first()
        if not confirmed:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not found'}
            }, status=404)

        if request.GET.get('delete'):
            if confirmed.user_id == serializer.validated_data['client_id']:
                confirmed.friend_confirmed_children = False
            elif confirmed.friend_id == serializer.validated_data['client_id']:
                confirmed.user_confirmed_children = False
        else:
            if confirmed.user_id == serializer.validated_data['client_id']:
                confirmed.friend_confirmed_children = True
            elif confirmed.friend_id == serializer.validated_data['client_id']:
                confirmed.user_confirmed_children = True

        confirmed.save(update_fields=(['user_confirmed_children', 'friend_confirmed_children']))
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)
