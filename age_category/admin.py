from django.contrib import admin
from django.contrib.admin import display

from age_category.models import AgeCategory, AgeCategoryRelations


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'age', 'get_name_category']

    @display(description='Name Category')
    def get_name_category(self, obj):
        return [categories.name for categories in obj.relations.all()]


class CategoryRelationsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'get_name_course']

    @display(description='Name Course')
    def get_name_course(self, obj):
        return [course.name for course in obj.relations.all()]


admin.site.register(AgeCategory, CategoryAdmin)
admin.site.register(AgeCategoryRelations, CategoryRelationsAdmin)