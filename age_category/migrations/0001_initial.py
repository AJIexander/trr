# Generated by Django 4.1.1 on 2023-01-10 13:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('assignments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgeCategoryRelations',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('relations', models.ManyToManyField(related_name='course', to='assignments.course')),
            ],
        ),
        migrations.CreateModel(
            name='AgeCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('age', models.IntegerField(default=None, null=True)),
                ('relations', models.ManyToManyField(related_name='category_course', to='age_category.agecategoryrelations')),
            ],
        ),
    ]
