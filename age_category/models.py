from django.db import models

from assignments.models import Course


class AgeCategoryRelations(models.Model):
    class Meta:
        verbose_name_plural = 'Категорії курсів'
    name = models.CharField(max_length=50, verbose_name='Назва')
    relations = models.ManyToManyField(Course, related_name='course', verbose_name='Курси')

    def __str__(self):
        return self.name

    def to_dict(self):
        return {
            'category_name': self.name,
            'courses_name': [curs.name for curs in self.relations.all()]
        }


class AgeCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Вікові категорії'
    age = models.IntegerField(null=True, default=None, verbose_name='Вік')
    relations = models.ManyToManyField(AgeCategoryRelations, related_name='category_course',
                                       verbose_name='Категорії курсів')
