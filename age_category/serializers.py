from rest_framework import serializers


class CategoryGetSerializer(serializers.Serializer):
    age = serializers.IntegerField(required=True)