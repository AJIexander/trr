from django.urls import path
from . import views

urlpatterns = [
    path('get_category/<int:age>/', views.CategoryOne.as_view(), name='get_category'),
    path('get_category_all/', views.CategoryAll.as_view(), name='get_category_all'),
]