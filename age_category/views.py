from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import AgeCategory
from .serializers import CategoryGetSerializer


class CategoryAll(APIView):
    """
    Receiving all age recommendations
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        queryset = AgeCategory.objects.all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No assign category'}}, status=404)
        data = []
        for query in queryset:
            data.append({
                'id': query.id,
                'age': query.age,
                'category': [name.to_dict() for name in query.relations.all()],
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class CategoryOne(APIView):
    """
    Obtaining an age recommendation
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, age: int):
        serializer = CategoryGetSerializer(data={'age': age})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = AgeCategory.objects.filter(age=serializer.validated_data['age']).first()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No assign category'}}, status=404)

        data = [{
            'id': queryset.id,
            'age': queryset.age,
            'category': [name.to_dict() for name in queryset.relations.all()]
        }]
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)
