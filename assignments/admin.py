from django import forms
from django.contrib import admin

from assignments.models import Course, TaskProgress, Task, ChildrenCourseRelation, TasksCourseRelation, \
    UserCommentCourse, UserSurveyTask, MethodologiesCategory, SubCategoryCourse
from ckeditor_uploader.widgets import CKEditorUploadingWidget

# Register your models here.


class CourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'sub_category', 'paid_content']


class TaskAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = Task
        fields = '__all__'


class TaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'score', 'currency', 'content', 'task_image_one', 'task_sound_one', 'task_image_two',
                    'task_sound_two', 'task_image_three', 'task_sound_three',
                    'task_image_four', 'task_sound_four',  'task_image_five', 'task_sound_five',
                    'task_image_true', 'task_sound_true']
    form = TaskAdminForm


class TaskProgressAdmin(admin.ModelAdmin):
    list_display = ['id', 'children', 'task_relation', 'task_status', 'created_ad', 'finished_ad']


class ChildrenCourseRelationAdmin(admin.ModelAdmin):
    list_display = ['id', 'children', 'course', 'created_ad', 'finished_ad']


class TasksCourseRelationAdmin(admin.ModelAdmin):
    list_display = ['id', 'course_id', 'task_id']
    list_display_links = ['course_id', 'task_id']


class UserSurveyTaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'content', 'course']
    form = TaskAdminForm


class UserCommentCourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'survey_course', 'children_id', 'user', 'comment', 'score']


class MethodologiesCategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'content']
    form = TaskAdminForm


class SubCategoryCourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'content', 'methodologies']
    form = TaskAdminForm


admin.site.register(Course, CourseAdmin)
admin.site.register(TaskProgress, TaskProgressAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(ChildrenCourseRelation, ChildrenCourseRelationAdmin)
admin.site.register(TasksCourseRelation, TasksCourseRelationAdmin)
admin.site.register(UserCommentCourse, UserCommentCourseAdmin)
admin.site.register(UserSurveyTask, UserSurveyTaskAdmin)
admin.site.register(MethodologiesCategory, MethodologiesCategoryAdmin)
admin.site.register(SubCategoryCourse, SubCategoryCourseAdmin)

