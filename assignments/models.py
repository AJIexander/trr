from datetime import datetime
from django.db import models
from accounts.models import ChildrenProfile, User
from Kiddisvit.settings import DOMAIN


class MethodologiesCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Методології розвитку'

    name = models.CharField(max_length=50, verbose_name='Назва')
    content = models.TextField(verbose_name='Контент')

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class SubCategoryCourse(models.Model):
    class Meta:
        verbose_name_plural = 'Типи интелекту'

    name = models.CharField(max_length=50, verbose_name='Назва')
    content = models.TextField(verbose_name='Контент')
    methodologies = models.ForeignKey(MethodologiesCategory, on_delete=models.CASCADE, related_name='methodologies_rel',
                                      default=None, null=True, verbose_name='Методологія')

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Course(models.Model):
    class Meta:
        verbose_name_plural = 'Курси'

    name = models.CharField(max_length=50, verbose_name='Назва')
    sub_category = models.ForeignKey(SubCategoryCourse, on_delete=models.CASCADE, related_name='course_rels',
                                     default=None, null=True, verbose_name='Тип интелекту')
    paid_content = models.BooleanField(default=False, verbose_name='Платний контент')

    def __str__(self):
        return self.name

    def get_all_tasks_id(self):
        return [rel.task_id.id for rel in self.started_tasks.all()]

    def get_all_tasks_names(self):
        return [rel.task_id.name for rel in self.started_tasks.all()]

    def get_progress(self):
        in_progress = 0
        done = 0
        for task in self.tasks.prefetch_related('progress').all():
            if task.progress.first().task_status == TaskProgress.statuses[0][0]:
                in_progress += 1
            elif task.progress.first().task_status == TaskProgress.statuses[1][0]:
                done += 1
        return in_progress, done, self.tasks.count()

    def get_score_for_child(self, child_id: int):
        score = 0
        for task_rel in self.started_tasks.all():
            score += sum(
                [progress.score for progress in
                 task_rel.progress.filter(children_id=child_id).all() if progress.finished_ad]
            )
        return score


class Task(models.Model):
    class Meta:
        verbose_name_plural = 'Завдання'

    name = models.CharField(max_length=50, verbose_name='Назва')
    score = models.IntegerField(default=0, verbose_name='Кол во балів за завдання')
    content = models.TextField(verbose_name='Контент')
    currency = models.IntegerField(default=0, verbose_name='Внутршньо сервісна валюта')
    task_image_one = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                       verbose_name='Перша картинка')
    task_image_two = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                       verbose_name='Друга картинка')
    task_image_three = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                         verbose_name='Третя картинка')
    task_image_four = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                        verbose_name='Четверта картинка')
    task_image_five = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                        verbose_name="П'ята картинка")
    task_image_true = models.ImageField(null=True, default=None, blank=True, upload_to='task_media/',
                                        verbose_name='Правильна відповідь')
    task_sound_one = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                      verbose_name='Звук для першої картинки')
    task_sound_two = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                      verbose_name='Звук для другої картинки')
    task_sound_three = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                        verbose_name='Звук для третьої картинки')
    task_sound_four = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                       verbose_name='Звук для четвертої картинки')
    task_sound_five = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                       verbose_name="Звук для п'ятої картинки")
    task_sound_true = models.FileField(null=True, default=None, blank=True, upload_to='task_media/',
                                       verbose_name='Звук для правельної відповіді')

    def __str__(self):
        return self.name

    def get_images_and_sounds(self):
        return {
            'task_image_one': f'{DOMAIN}{self.task_image_one.url}' if self.task_image_one else None,
            'task_sound_one': f'{DOMAIN}{self.task_sound_one.url}' if self.task_sound_one else None,
            'task_image_two': f'{DOMAIN}{self.task_image_two.url}' if self.task_image_two else None,
            'task_sound_two': f'{DOMAIN}{self.task_sound_two.url}' if self.task_sound_two else None,
            'task_image_three': f'{DOMAIN}{self.task_image_three.url}' if self.task_image_three else None,
            'task_sound_three': f'{DOMAIN}{self.task_sound_three.url}' if self.task_sound_three else None,
            'task_image_four': f'{DOMAIN}{self.task_image_four.url}' if self.task_image_four else None,
            'task_sound_four': f'{DOMAIN}{self.task_sound_four.url}' if self.task_sound_four else None,
            'task_image_five': f'{DOMAIN}{self.task_image_five.url}' if self.task_image_five else None,
            'task_sound_five': f'{DOMAIN}{self.task_sound_five.url}' if self.task_sound_five else None,
            'task_image_true': f'{DOMAIN}{self.task_image_true.url}' if self.task_image_true else None,
            'task_sound_true': f'{DOMAIN}{self.task_sound_true.url}' if self.task_sound_true else None,
        }


class TasksCourseRelation(models.Model):
    class Meta:
        verbose_name_plural = 'Додання завдань до курсу'

    course_id = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='started_tasks', verbose_name='Курс')
    task_id = models.ForeignKey(Task, on_delete=models.CASCADE, related_name='course_relations',
                                verbose_name='Завдання')

    def __str__(self):
        return self.task_id.name


class TaskProgress(models.Model):
    class Meta:
        verbose_name_plural = 'Прогресс проходження завдань'

    statuses = (
        ('assign', 'Assign'),
        ('in_progress', 'In progress'),
        ('done', 'Done')
    )
    task_relation = models.ForeignKey(TasksCourseRelation, on_delete=models.CASCADE, related_name='progress',
                                      verbose_name='Курс')
    created_ad = models.DateTimeField(null=True, auto_now_add=True, verbose_name='Час додання курсу для проходження')
    finished_ad = models.DateTimeField(null=True, default=None, verbose_name='Час завершення завдання')
    task_status = models.CharField(max_length=50, choices=statuses, default=statuses[0][0],
                                   verbose_name='Статус завдання')
    children = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE,
                                 related_name='children_task_progress_rels', verbose_name='Дитина')
    score = models.IntegerField(null=True, default=None)

    def __str__(self):
        return self.task_relation.task_id.name

    def change_status_task(self, status: str = statuses[1][0]) -> None:
        self.task_status = status
        if status == self.statuses[2][0]:
            self.children.children_score.add_score(self.score)
        elif status == self.statuses[1][0]:
            pass
        else:
            raise Exception('Bad status')
        self.save(update_fields=['task_status'])

    def is_all_tasks_done(self):
        tasks = TaskProgress.objects.filter(children=self.children_id,
                                            task_relation__course_id=self.task_relation.course_id).all()
        return all([task.task_status == 'done' for task in tasks])


class ChildrenCourseRelation(models.Model):
    class Meta:
        verbose_name_plural = 'Прогресс проходження курсів'

    children = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE, related_name='course_rels',
                                 verbose_name='Дитина')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='children_rels', verbose_name='Курс')
    created_ad = models.DateTimeField(null=True, auto_now_add=True, verbose_name='Час додання курсу для проходження')
    finished_ad = models.DateTimeField(null=True, default=None, verbose_name='Час завершення курсу')

    def finish(self):
        self.finished_ad = datetime.now()
        self.save(update_fields=['finished_ad'])


class UserSurveyTask(models.Model):
    class Meta:
        verbose_name_plural = 'Завдання для батьків'

    name = models.CharField(max_length=50, verbose_name='Назва')
    content = models.TextField(verbose_name='Контент')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='description_course',
                               verbose_name='Курс')

    def __str__(self):
        return self.name


class UserCommentCourse(models.Model):
    class Meta:
        verbose_name_plural = 'Коментарі про курс'

    survey_course = models.ForeignKey(UserSurveyTask, on_delete=models.CASCADE, related_name='comment',
                                      verbose_name='Курс')
    children_id = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE, related_name='children_comment',
                                    verbose_name='Дитина')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_comment', verbose_name='Юзер')
    comment = models.CharField(max_length=150, verbose_name='Коментар')
    score = models.IntegerField(default=0, verbose_name='Оцінка')

    def __str__(self):
        return self.comment
