from rest_framework import serializers


class AddCourseSerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)
    course_id = serializers.IntegerField(required=True)


class TaskStartSerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=False)
    task_id = serializers.IntegerField(required=True)
    course_id = serializers.IntegerField(required=True)


class GetProgressSerializers(serializers.Serializer):
    children_id = serializers.IntegerField(required=False)


class UserSurveySerializer(serializers.Serializer):
    course_id = serializers.IntegerField(required=True)


class FinishSurveySerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)
    survey_course_id = serializers.IntegerField(required=True)
    comment = serializers.CharField(required=True)
    score = serializers.IntegerField(required=True)


class TaskFinishSerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=False)
    task_id = serializers.IntegerField(required=True)
    course_id = serializers.IntegerField(required=True)
    score = serializers.IntegerField(required=True)
    currency = serializers.IntegerField(required=True)
