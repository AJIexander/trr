from django.urls import path
from . import views

urlpatterns = [
    path('assign_course/', views.AssignCourse.as_view(), name='assign_course'),
    path('start_task/', views.StartTask.as_view(), name='start_task'),
    path('add_score_end_task/', views.FinishTask.as_view(), name='add_score_end_task'),
    path('get_course/<int:children_id>/', views.GetAllCourse.as_view(), name='get_course'),
    path('get_progress/<int:children_id>/', views.GetProgressTask.as_view(), name='get_progress'),
    path('start_survey_task/', views.StartUserSurveyCourseTask.as_view(), name='start_survey_task'),
    path('finish_survey_task/', views.FinishUserSurveyCourseTask.as_view(), name='finish_survey_task'),
    path('get_all_sub_cat/', views.GetAllSubCategory.as_view(), name='get_all_sub_cat'),
    path('get_recommendation/<int:children_id>/', views.RecommendationCourse.as_view(),
         name='get_recommendation'),
]