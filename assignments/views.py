import datetime

from django.db.models import Q
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from assignments.models import TaskProgress, Course, TasksCourseRelation, ChildrenCourseRelation, \
    UserSurveyTask, UserCommentCourse, SubCategoryCourse
from assignments.serializers import TaskStartSerializer, AddCourseSerializer, GetProgressSerializers, \
    UserSurveySerializer, FinishSurveySerializer, TaskFinishSerializer
from accounts.models import ChildrenProfile, CurrencyWallet, UserProfile
from parents_test.models import ParenTestScore
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


class AssignCourse(APIView):
    """
    Documentation endpoint
    Assigning a course to a child, both parent and child can assign a course, when adding a course to a child all
    assignments of this have assignment status
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=AddCourseSerializer)
    def post(self, request):
        serializer = AddCourseSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        course = Course.objects.filter(id=serializer.data['course_id']).first()
        if not course:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not course'}}, status=404)

        child = ChildrenProfile.objects.filter(
            Q(children_user__parent=request.user.pk) | Q(children_user_id=request.user.pk),
            id=serializer.data['children_id']).first()
        if not child:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        if course.paid_content is True:
            prime = UserProfile.objects.filter(client=request.user).first()
            if prime.prime_account is True:
                child.assign_to_course(course=course)
            else:
                return Response({
                    'status': 'Error',
                    'code': 402,
                    'data': {'description': 'Payment required'}}, status=402)
        else:
            child.assign_to_course(course=course)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Course has been added for passing'}}, status=201)


class StartTask(APIView):
    """
    Documentation endpoint
    Start a course task, only the child can start a task, as well as the status of the task that has been started is
    transferred from "assign" to "in progress"
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=TaskStartSerializer)
    def post(self, request):
        serializer = TaskStartSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children = ChildrenProfile.objects.filter(Q(children_user__parent=request.user.pk,
                                                    id=serializer.validated_data.get('children_id'))
                                                  | Q(children_user_id=request.user.pk)).first()
        if not children:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        progress = TaskProgress.objects.filter(children=children,
                                               task_relation__task_id=serializer.data['task_id'],
                                               task_relation__course_id=serializer.data['course_id'],
                                               task_status='assign').first()
        if not progress:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Task progress not found'}}, status=404)

        progress.change_status_task(status='in_progress')
        progress.save()
        data = {
            'task_name': progress.task_relation.task_id.name,
            'task_content': progress.task_relation.task_id.content,
            'task_score': progress.task_relation.task_id.score,
            'images_task': progress.task_relation.task_id.get_images_and_sounds(),
            'task_currency': progress.task_relation.task_id.currency,
        }
        return Response({
            'status': 'Success',
            'code': 201,
            'data': data
        }, status=201)


class FinishTask(APIView):
    """
    Documentation endpoint
    Completion of course tasks, only the child can complete the course, as well as if the task is completed application
    transfers to the child's account points for the task, also checks whether all tasks are completed, if all tasks are
    completed, the course is transferred to the status of "Done"
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=TaskFinishSerializer)
    def post(self, request):
        serializer = TaskFinishSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        children = ChildrenProfile.objects.filter(Q(children_user__parent=request.user.pk,
                                                    id=serializer.validated_data.get('children_id'))
                                                  | Q(children_user_id=request.user.pk)).first()
        if not children:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        progress = TaskProgress.objects.filter(children=children,
                                               task_relation__task_id=serializer.data['task_id'],
                                               task_status='in_progress',
                                               task_relation__course_id=serializer.data['course_id']).first()
        progress.score = serializer.validated_data.get('score')
        progress.finished_ad = datetime.datetime.now()
        progress.change_status_task(status='done')
        if progress.is_all_tasks_done():
            course = children.course_rels.filter(course_id=progress.task_relation.course_id,
                                                 finished_ad=None).first()
            course.finish()
        wallet = CurrencyWallet.objects.filter(user=children.children_user).first()
        if wallet:
            wallet.currency += serializer.validated_data['currency']
        else:
            wallet = CurrencyWallet.objects.create(user=children.children_user,
                                                   currency=serializer.validated_data['currency'])
        wallet.save()
        progress.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': progress.task_status
        }, status=201)


class GetAllCourse(APIView):
    """
    Getting all courses and added tasks
    """

    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetProgressSerializers(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = TasksCourseRelation.objects.select_related('course_id__sub_category__methodologies')
        if not queryset.exists():
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'There are no courses that have added tasks'}
            }, status=404)

        completed_courses = request.GET.get('completed_courses')
        if completed_courses == 'true':
            queryset = queryset.filter(
                course_id__children_rels__children_id=serializer.validated_data['children_id']
            )
        elif completed_courses == 'false':
            queryset = queryset.exclude(
                course_id__children_rels__children_id=serializer.validated_data['children_id']
            )
        sub_category_id = request.GET.get('sub_category_id')
        if sub_category_id:
            queryset = queryset.filter(course_id__sub_category_id=sub_category_id)

        courses = {}
        for relation in queryset:
            course = relation.course_id
            if course.name not in courses:
                courses[course.name] = {
                    'curs_id': course.id,
                    'curs_name': course.name,
                    'paid_content': course.paid_content,
                    'sub_category_id': course.sub_category.id,
                    'sub_category': course.sub_category.name,
                    'methodologies': course.sub_category.methodologies.name,
                    'curs_done': False,
                    'tasks': {
                        'task_id': [],
                        'task_name': []
                    }
                }
            if course.children_rels.filter(children_id=children_id).exists():
                courses[course.name]['curs_done'] = True
            courses[course.name]['tasks']['task_id'].extend(course.get_all_tasks_id())
            courses[course.name]['tasks']['task_name'].extend(course.get_all_tasks_names())

        paginator = Paginator(list(courses.values()), request.GET.get('list_object', 10))
        page = request.GET.get('page', 1)
        try:
            courses_page = paginator.page(page)
        except (PageNotAnInteger, EmptyPage):
            courses_page = paginator.page(1)

        pagination = {
            'current_page': courses_page.number,
            'total_pages': paginator.num_pages,
            'has_next': courses_page.has_next(),
            'has_previous': courses_page.has_previous()
        }

        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'curses': courses_page.object_list,
                'pagination': pagination
            }
        }, status=200)


class GetProgressTask(APIView):
    """
    Check progress
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetProgressSerializers(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        permission = ChildrenProfile.objects.filter(Q(children_user__parent=request.user.pk) | Q(
            children_user_id=request.user.pk)).first()
        if not permission:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No right'}}, status=401)

        queryset = ChildrenCourseRelation.objects.filter(
            children=serializer.data['children_id']).all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No courses started or completed'}}, status=404)

        data = []
        for progress in queryset:
            data.append({
                'id': progress.course.id,
                'course': progress.course.name,
                'sub_category_name': progress.course.sub_category.name,
                'methodologies_name': progress.course.sub_category.methodologies.name,
                'time_start': progress.created_ad,
                'time_end': progress.finished_ad,
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class StartUserSurveyCourseTask(APIView):
    """
    User start survey task
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=UserSurveySerializer)
    def post(self, request):
        serializer = UserSurveySerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        course = UserSurveyTask.objects.filter(course__id=serializer.validated_data['course_id']).first()
        if not course:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No assignments added to this course'}}, status=404)

        data = {
            'survey_id': course.id,
            'name': course.name,
            'content': course.content
        }
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class FinishUserSurveyCourseTask(APIView):
    """
    User finish survey task, add user comment for db
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=FinishSurveySerializer)
    def post(self, request):
        serializer = FinishSurveySerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        UserCommentCourse.objects.create(children_id_id=serializer.validated_data['children_id'],
                                         survey_course_id=serializer.validated_data['survey_course_id'],
                                         comment=serializer.validated_data['comment'],
                                         score=serializer.validated_data['score'],
                                         user_id=request.user.pk)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Done!'}}, status=201)


class GetAllSubCategory(APIView):
    """
    Getting all types of intelligence
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        queryset = SubCategoryCourse.objects.all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No assignments intelligence types'}}, status=404)

        data = []
        for query in queryset:
            data.append({
                'id': query.id,
                'sub_category_name': query.name,
                'methodologies_id': query.methodologies.id,
                'methodologies_name': query.methodologies.name
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class RecommendationCourse(APIView):
    """
    Return of course recommendations
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = GetProgressSerializers(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = ParenTestScore.objects.filter(child_id=serializer.validated_data['children_id']).order_by(
            '-score', 'sub_category_name')[:]

        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No completed parenting courses'}
            }, status=404)

        cur = []
        for query in queryset:
            courses = query.sub_category_name.course_rels.all()
            if request.GET.get('completed_courses') == 'true':
                courses = courses.filter(children_rels__children_id=serializer.validated_data['children_id'])
            elif request.GET.get('completed_courses') == 'false':
                courses = courses.exclude(children_rels__children_id=serializer.validated_data['children_id'])
            cur.extend(courses)

        paginator = Paginator(cur, request.GET.get('list_objct', 10))
        page = request.GET.get('page')
        try:
            cur = paginator.page(page)
        except (PageNotAnInteger, EmptyPage):
            cur = paginator.page(1)

        curses = [{'curs_id': curs.id,
                   'curs_name': curs.name,
                   'curs_done': bool(
                       curs.children_rels.filter(
                           children_id=serializer.data['children_id']).first().finished_ad
                       if curs.children_rels.filter(
                           children_id=serializer.data['children_id']).first() else None),
                   'paid_content': curs.paid_content,
                   'sub_category_id': curs.sub_category.id,
                   'sub_category_name': curs.sub_category.name,
                   'tasks': {'task_id': curs.get_all_tasks_id(),
                             'task_name': curs.get_all_tasks_names()}}
                  for curs in cur]

        pagination = {
            'current_page': cur.number,
            'total_pages': paginator.num_pages,
            'has_next': cur.has_next(),
            'has_previous': cur.has_previous()}

        return Response({
            'status': 'Success',
            'code': 200,
            'data': {'curses': curses, 'pagination': pagination},
        }, status=200)

