from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.contrib.admin import display

from blog.models import MultiCategory, Category, MultiCategoryRelation, State, ArticleReview


class MultiCategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description']


class MultiCategoryRelationAdmin(admin.ModelAdmin):
    list_display = ['id', 'multi_category', 'get_name_category']

    @display(description='Name Category')
    def get_name_category(self, obj):
        return [categories.name for categories in obj.category.all()]


class StateAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = State
        fields = '__all__'


class StateAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'content', 'category', 'multi_category', 'author', 'paid_content']
    form = StateAdminForm


class ArticleReviewAdmin(admin.ModelAdmin):
    list_display = ['id', 'state_review', 'user', 'favorite']


admin.site.register(MultiCategory, MultiCategoryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(MultiCategoryRelation, MultiCategoryRelationAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(ArticleReview, ArticleReviewAdmin)