# Generated by Django 4.1.1 on 2023-04-30 20:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_alter_articlereview_options_alter_category_options_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='state',
            name='paid_content',
            field=models.BooleanField(default=False, verbose_name='Платний контент'),
        ),
    ]
