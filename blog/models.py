from django.db import models

from accounts.models import User


class MultiCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Мульти категорії'
    name = models.CharField(max_length=50, verbose_name='Назва')
    description = models.CharField(max_length=50, verbose_name='Опис')

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Категорії'
    name = models.CharField(max_length=50, verbose_name='Назва')
    description = models.CharField(max_length=50, verbose_name='Опис')

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class MultiCategoryRelation(models.Model):
    class Meta:
        verbose_name_plural = 'Додання категорії до мульти категорії'
    multi_category = models.ForeignKey(MultiCategory, on_delete=models.CASCADE, related_name='multi_category_relations',
                                       verbose_name='Мульти категорія')
    category = models.ManyToManyField(Category, related_name='category_relations_relations', verbose_name='Категорія')

    def __str__(self):
        return self.multi_category.name

    def __repr__(self):
        return self.multi_category.name


class State(models.Model):
    class Meta:
        verbose_name_plural = 'Статьі'
    title = models.CharField(max_length=50, verbose_name='Заголовок')
    content = models.TextField()
    multi_category = models.ForeignKey(MultiCategoryRelation, on_delete=models.CASCADE, related_name='states_multi',
                                       verbose_name='Мульти Категорія')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='states_category',
                                 verbose_name='Категорія')
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user', default='Administrator',
                               verbose_name='Автор')
    paid_content = models.BooleanField(default=False, verbose_name='Платний контент')

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title


class ArticleReview(models.Model):
    class Meta:
        verbose_name_plural = 'Огляд статей'
    state_review = models.ForeignKey(State, on_delete=models.CASCADE, related_name='states', verbose_name='Статья')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='review', verbose_name='Юзер')
    favorite = models.BooleanField(null=True, default=None, verbose_name='Додано в улюблене')

    def __str__(self):
        return self.state_review.title