from rest_framework import serializers


class GetStateSerializer(serializers.Serializer):
    state_id = serializers.IntegerField(required=True)


class GetMultiCategorySerializer(serializers.Serializer):
    multi_category_id = serializers.IntegerField(required=True)