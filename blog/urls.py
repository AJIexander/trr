from django.urls import path
from . import views

urlpatterns = [
    path('get_state/<int:state_id>/', views.GetState.as_view(), name='get_state'),
    path('get_all_state/', views.GetAllState.as_view(), name='get_all_state'),
    path('add_favorite_state/', views.AddFavoriteState.as_view(), name='add_favorite_state'),
    path('get_favorite_state/', views.GetFavoriteState.as_view(), name='get_favorite_state'),
    path('delete_favorite_state/', views.DeleteUserFavoriteState.as_view(), name='delete_favorite_state'),
    path('get_multi_all_state/<int:multi_category_id>/', views.GetAllStatesOneMultiCategory.as_view(),
         name='get_multi_all_state'),
    path('get_all_multi/', views.GetAllMultiCategory.as_view(), name='get_all_multi')
]