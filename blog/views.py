from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import UserProfile
from blog.models import State, ArticleReview, MultiCategory
from blog.serializers import GetStateSerializer, GetMultiCategorySerializer


class GetState(APIView):
    """
    Documentation endpoint
    Getting an article by its individual id
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, state_id: int):
        serializer = GetStateSerializer(data={'state_id': state_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        state = State.objects.filter(id=serializer.validated_data['state_id']).first()
        if not state:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not state'}}, status=404)

        favorite_state = ArticleReview.objects.filter(user=request.user.pk, favorite=True,
                                                      state_review=state).first()
        data = {
            'id': state.id,
            'title': state.title,
            'content': state.content,
            'category_id': state.category.id,
            'category': state.category.name,
            'multi_category_id': state.multi_category.multi_category.id,
            'multi_category': state.multi_category.multi_category.name,
            'author': state.author.username,
            'favorite_state': favorite_state.favorite if favorite_state else None,
            'paid_content': state.paid_content
        }
        ArticleReview.objects.create(user_id=request.user.pk, state_review_id=state.id)
        if state.paid_content is True:
            prime = UserProfile.objects.filter(client=request.user).first()
            if prime.prime_account is True:
                return Response({
                    'status': 'Success',
                    'code': 200,
                    'data': data
                }, status=200)
            else:
                return Response({
                    'status': 'Error',
                    'code': 402,
                    'data': {'description': 'Payment required'}}, status=402)

        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetAllState(APIView):
    """
    Documentation endpoint
    Receiving all articles
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        states = State.objects.all()
        if not states:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not state'}}, status=404)
        data = []
        for state in states:
            data.append({
                'id': state.id,
                'title': state.title,
                'category_id': state.category.id,
                'category': state.category.name,
                'multi_category_id': state.multi_category.multi_category.id,
                'multi_category': state.multi_category.multi_category.name,
                'author': state.author.username,
                'paid_content': state.paid_content})
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class AddFavoriteState(APIView):
    """
    Add favorite state
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=GetStateSerializer)
    def post(self, request):
        serializer = GetStateSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        favorite_state = ArticleReview.objects.filter(state_review=serializer.validated_data['state_id'],
                                                      user=request.user.pk).first()
        if not favorite_state:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'State not review state'}}, status=404)

        favorite_state.favorite = True
        favorite_state.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': f'State {favorite_state.state_review.title} add you favorite'}}, status=201)


class GetFavoriteState(APIView):
    """
    Get all favorite state
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        favorite_state = ArticleReview.objects.filter(user=request.user.pk, favorite=True).all()
        if not favorite_state:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': "You don't have any favorite articles"}}, status=404)

        data = []
        for favorite in favorite_state:
            data.append({
                'state_id': favorite.state_review.id,
                'title': favorite.state_review.title,
                'category': favorite.state_review.category.name,
                'multi_category': favorite.state_review.multi_category.multi_category.name,
                'paid_content': favorite.state_review.paid_content
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class DeleteUserFavoriteState(APIView):
    """
    Delete user favorite state
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=GetStateSerializer)
    def post(self, request):
        serializer = GetStateSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        favorite_state = ArticleReview.objects.filter(user=request.user.pk, favorite=True,
                                                      state_review=serializer.validated_data['state_id']).first()
        if not favorite_state:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': "State not found"}}, status=404)

        favorite_state.favorite = False
        favorite_state.save()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {'description': 'State cancel favorite list'}}, status=200)


class GetAllStatesOneMultiCategory(APIView):
    """
    Get states one multi_category
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, multi_category_id: int):
        serializer = GetMultiCategorySerializer(data={'multi_category_id': multi_category_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = State.objects.filter(
            multi_category__multi_category=serializer.validated_data['multi_category_id']).all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': "State not found"}}, status=404)

        data = []
        for states in queryset:
            data.append({
                'id': states.id,
                'title': states.title,
                'category': states.category.name,
                'paid_content': states.paid_content
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetAllMultiCategory(APIView):
    """
    Get all multi_category
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        queryset = MultiCategory.objects.all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': "Multi category not found"}}, status=404)

        data = []
        for query in queryset:
            data.append({
                'id': query.id,
                'multi_name': query.name,
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)
