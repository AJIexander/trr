from django.db import models

from accounts.models import User


class Chat(models.Model):
    name = models.CharField(max_length=50)
    created_ad = models.DateTimeField(null=True, auto_now_add=True)

    def is_owner(self, user_id: int):
        return self.members.filter(user__id=user_id).first().is_owner

    def is_member(self, user_id: int):
        return self.members.filter(user__id=user_id).first()

    def is_moderator(self, user_id: int):
        return self.members.filter(user__id=user_id).first()


class ChatMessage(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='users')
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='chats')
    message = models.TextField(null=True, default=None)
    image = models.ImageField(null=True, default=None)
    file = models.FileField(null=True, default=None)
    created_ad = models.DateTimeField(null=True, auto_now_add=True)
    edit = models.DateTimeField(null=True, default=None)
    redirect_message = models.ForeignKey('self', on_delete=models.CASCADE, related_name='redirected_messages',
                                         null=True, default=None)


class ChatMember(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='member')
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='members')
    is_owner = models.BooleanField(default=False)
    is_moderator = models.BooleanField(default=False)
