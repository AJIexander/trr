from rest_framework import serializers


class CreateChatSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    members = serializers.ListField(child=serializers.CharField())


class AssignChatSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)
    members = serializers.ListField(child=serializers.CharField())


class SendMessageSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)
    message = serializers.CharField(required=False)
    image = serializers.ImageField(required=False)
    file = serializers.FileField(required=False)


class GetAllMessageSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)


class DeleteMessageSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)
    message_id = serializers.IntegerField(required=True)


class EditMessageSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)
    message_id = serializers.IntegerField(required=True)
    message = serializers.CharField(required=True)


class AddModerationSerializer(serializers.Serializer):
    chat_id = serializers.IntegerField(required=True)
    members_id = serializers.IntegerField(required=True)
