from django.urls import path

from . import views

urlpatterns = [
    path('get_members/', views.GetMembers.as_view(), name='get_members'),
    path('create_chat/', views.CreateChat.as_view(), name='create_chat'),
    path('assign_user_in_chat/', views.AssignUserChat.as_view(), name='assign_user_in_chat'),
    path('send_message/', views.SendMessageChat.as_view(), name='send_message'),
    path('get_all_message/<int:chat_id>/', views.GetAllMessageChat.as_view(), name='get_all_message'),
    path('delete_message/', views.DeleteMessageChat.as_view(), name='delete_message'),
    path('edit_message/', views.EditMessageChat.as_view(), name='edit_message'),
    path('add_moderation/', views.ModerationChat.as_view(), name='add_moderation'),
    path('delete_user_from_chat/', views.DeleteUserChat.as_view(), name='delete_user_from_chat'),
    path('get_all_members_in_chat/<int:chat_id>/', views.GetAllMembersChat.as_view(), name='get_all_members_in_chat'),
    path('redirect_message/', views.RedirectMessageChat.as_view(), name='redirect_message'),
    ]