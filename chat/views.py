import datetime
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.models import User
from chat.models import Chat, ChatMember, ChatMessage
from chat.serializers import CreateChatSerializer, AssignChatSerializer, SendMessageSerializer, GetAllMessageSerializer, \
    DeleteMessageSerializer, EditMessageSerializer, AddModerationSerializer
from django.core.files.storage import FileSystemStorage


class GetMembers(APIView):
    """
    Documentation endpoint
    Retrieve all users that belong to the user that will be retrieved from request
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        queryset = User.objects.filter(parent_id=request.user.pk).all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No users'}}, status=404)

        member = []
        for user in queryset:
            member.append({
                'id': user.id,
                'username': user.username,
                'email': user.email})
        return Response({
            'status': 'Success',
            'code': 200,
            'data': member
        }, status=200)


class CreateChat(APIView):
    """
    Documentation endpoint
    Creating a chat room, the user who started this endpoint becomes the owner of the chat room, he sets the name and
    adds the users he gets from the get_members/ endpoint, the user can add either one user or multiple users
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=CreateChatSerializer)
    def post(self, request):
        serializer = CreateChatSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.create(name=serializer.validated_data['name'])
        members = [ChatMember(user_id=request.user.pk, is_owner=True, is_moderator=True, chat=chat)] + [
            ChatMember(user_id=int(user_id), chat=chat) for user_id in
            serializer.validated_data['members'][0].split(',')
        ]
        ChatMember.objects.bulk_create(members, ignore_conflicts=True)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Done'}}, status=201)


class AssignUserChat(APIView):
    """
    Documentation endpoint
    Adding a user/users to the chat room
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=AssignChatSerializer)
    def post(self, request):
        serializer = AssignChatSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if chat.is_owner(request.user.pk):
            members = [
                ChatMember(user_id=int(user_id), chat=chat)
                for user_id in serializer.validated_data['members'][0].split(',')
            ]
            ChatMember.objects.bulk_create(members, ignore_conflicts=True)
            return Response({
                'status': 'Success',
                'code': 200,
                'data': {'description': 'Users are added to the chat room'}}, status=200)
        elif chat.is_moderator(request.user.pk):
            members = [
                ChatMember(user_id=int(user_id), chat=chat)
                for user_id in serializer.validated_data['members'][0].split(',')
            ]
            ChatMember.objects.bulk_create(members, ignore_conflicts=True)
            return Response({
                'status': 'Success',
                'code': 200,
                'data': {'description': 'Users are added to the chat room'}}, status=200)
        else:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'No rights to add users to the chat room'}}, status=401)


class SendMessageChat(APIView):
    """
    Documentation endpoint
    Sending messages
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=SendMessageSerializer)
    def post(self, request):
        serializer = SendMessageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        folder = f'media/chat_media/{serializer.validated_data["chat_id"]}/'
        image = serializer.validated_data.get('image')
        file = serializer.validated_data.get('file')
        fi = FileSystemStorage(location=folder) if image else None
        ff = FileSystemStorage(location=folder) if file else None
        ChatMessage.objects.create(sender_id=request.user.pk,
                                   chat_id=serializer.validated_data['chat_id'],
                                   message=serializer.validated_data.get('message'),
                                   image=fi.save(image.name, image) if fi else None,
                                   file=ff.save(file.name, file) if ff else None)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Message sent'}}, status=201)


class GetAllMessageChat(APIView):
    """
    Documentation endpoint
    Retrieve all messages from the chat room
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, chat_id: int):
        serializer = GetAllMessageSerializer(data={'chat_id': chat_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'Not valid id chat'}}, status=400)

        if not chat.is_member(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not a member of the chat room'}}, status=401)

        data = []
        message_all = ChatMessage.objects.filter(chat=chat).all()
        for message in message_all:
            data.append({
                'id': message.id,
                'user': message.sender.username,
                'message': message.message if message.message else None,
                'image': message.image.url if message.image else None,
                'file': message.file.url if message.file else None,
                'create_ad': message.created_ad,
                'edit': message.edit
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class DeleteMessageChat(APIView):
    """
    Documentation endpoint
    Deleting a message from a chat room
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=DeleteMessageSerializer)
    def post(self, request):
        serializer = DeleteMessageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat.is_member(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not a member of the chat room'}}, status=401)

        ChatMessage.objects.filter(sender_id=request.user.pk, chat=chat,
                                   id=serializer.validated_data['message_id']).delete()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {'description': 'Chat deleted successfully'}}, status=200)


class EditMessageChat(APIView):
    """
    Documentation endpoint
    Editing Message
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=EditMessageSerializer)
    def post(self, request):
        serializer = EditMessageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat.is_member(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not a member of the chat room'}}, status=401)

        message = ChatMessage.objects.filter(sender_id=request.user.pk, chat=chat,
                                             id=serializer.validated_data['message_id']).first()
        message.message = serializer.validated_data['message']
        message.edit = datetime.datetime.now()
        message.save()
        data = [{
            'id': message.id,
            'user': message.sender.username,
            'message': message.message,
            'image': message.image.url if message.image else None,
            'file': message.file.url if message.file else None,
            'create_ad': message.created_ad,
            'edit': message.edit
        }]
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
    }, status=200)


class ModerationChat(APIView):
    """
    Documentation endpoint
    Adding a user to the "Moderator" role
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=AddModerationSerializer)
    def post(self, request):
        serializer = AddModerationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat.is_owner(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No rights'}}, status=401)

        ChatMember.objects.filter(user_id=serializer.validated_data['members_id']).update(is_moderator=True)
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Moderator role added'}}, status=201)


class DeleteUserChat(APIView):
    """
    Documentation endpoint
    Removing a user from the chat room. The "owner" can delete any username. The "Moderator" can delete a User that has
     no role. A "Moderator" cannot delete an "Owner" and cannot delete another "Moderator".
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=AddModerationSerializer)
    def post(self, request):
        serializer = AddModerationSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if chat.is_owner(request.user.pk):
            ChatMember.objects.filter(user_id=serializer.validated_data['members_id']).delete()
            return Response({
                'status': 'Success',
                'code': 200,
                'data': {'description': 'The user has been removed from the chat room'}}, status=200)

        elif chat.is_moderator(request.user.pk):
            member = ChatMember.objects.filter(user_id=serializer.validated_data['members_id']).first()
            if member.is_owner is False and member.is_moderator is False:
                member.delete()
                return Response({
                    'status': 'Success',
                    'code': 200,
                    'data': {'description': 'The user has been removed from the chat room'}}, status=200)

            else:
                return Response({
                    'status': 'Error',
                    'code': 400,
                    'data': {'description': 'User or moderator or chat owner'}}, status=400)

        else:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'No rights'}}, status=401)


class GetAllMembersChat(APIView):
    """
    Documentation endpoint
    Getting all chat users and their roles
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, chat_id: int):
        serializer = GetAllMessageSerializer(data={'chat_id': chat_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'Not valid id chat'}}, status=400)

        if not chat.is_member(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not a member of the chat room'}}, status=401)

        data = []
        members = ChatMember.objects.filter(chat=chat).all()
        for member in members:
            data.append({
                'user_id': member.user.id,
                'user': member.user.username,
                'owner': member.is_owner if member.is_owner is True else None,
                'moderator': member.is_moderator if member.is_moderator is True else None
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class RedirectMessageChat(APIView):
    """
    Documentation endpoint
    Redirect message
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=DeleteMessageSerializer)
    def post(self, request):
        serializer = DeleteMessageSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        chat = Chat.objects.filter(id=serializer.validated_data['chat_id']).first()
        if not chat:
            return Response({
                'status': 'Error',
                'code': 400,
                'data': {'description': 'Not valid id chat'}}, status=400)

        if not chat.is_member(request.user.pk):
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Not a member of the chat room'}}, status=401)

        ChatMessage.objects.create(sender_id=request.user.pk,
                                   redirect_message_id=serializer.validated_data['message_id'],
                                   chat=chat)
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {'description': 'Message redirect'}}, status=200)


