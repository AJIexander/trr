from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django import forms

from parents_test.models import ParentTask, ParentTaskProgress, ParenTestScore, ParentCourse, ParentTasksCourseRelation, \
    ParentScoreSubCategory


class ParentTaskAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())

    class Meta:
        model = ParentTask
        fields = '__all__'


class ParentCourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'content', 'method']
    form = ParentTaskAdminForm


class ParentTaskAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'content', 'currency', 'sub_cat']
    form = ParentTaskAdminForm


class ParentTaskProgressAdmin(admin.ModelAdmin):
    list_display = ['id', 'parent', 'parent_task_relation', 'task_status', 'created_ad', 'finished_ad']


class ParenTestScoreAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'child', 'sub_category_name', 'methodologies_name', 'score']


class ParentTasksCourseRelationAdmin(admin.ModelAdmin):
    list_display = ['id', 'course_id', 'task_id']
    list_display_links = ['course_id', 'task_id']


class ParentScoreSubCategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'sub_category_name', 'methodologies_name', 'score', 'child']


admin.site.register(ParentTaskProgress, ParentTaskProgressAdmin)
admin.site.register(ParentTask, ParentTaskAdmin)
admin.site.register(ParenTestScore, ParenTestScoreAdmin)
admin.site.register(ParentCourse, ParentCourseAdmin)
admin.site.register(ParentTasksCourseRelation, ParentTasksCourseRelationAdmin)
admin.site.register(ParentScoreSubCategory, ParentScoreSubCategoryAdmin)
