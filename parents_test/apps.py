from django.apps import AppConfig


class ParentsTestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'parents_test'
    verbose_name = 'Батьківське тестування'
