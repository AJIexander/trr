from django.db import models

from accounts.models import User, ChildrenProfile
from assignments.models import MethodologiesCategory, SubCategoryCourse


# Create your models here.

class ParentCourse(models.Model):
    class Meta:
        verbose_name_plural = 'Курси для батьків'

    name = models.CharField(max_length=50, verbose_name='Назва')
    content = models.TextField()
    method = models.ForeignKey(MethodologiesCategory, on_delete=models.CASCADE, related_name='meth_category',
                               default=None, null=True, verbose_name='Методологія')

    def __str__(self):
        return self.name

    def get_all_parent_tasks_id(self):
        return [rel.task_id.id for rel in self.started_tasks.all()]

    def get_all_parent_tasks_names(self):
        return [rel.task_id.name for rel in self.started_tasks.all()]

    def get_sub_cat_tasks(self):
        for rel in self.started_tasks.all():
            return rel.task_id.sub_cat.name

    def get_sub_id(self):
        for rel in self.started_tasks.all():
            return rel.task_id.sub_cat.id


class ParentTask(models.Model):
    class Meta:
        verbose_name_plural = 'Завдання для батьків'

    name = models.CharField(max_length=50, verbose_name='Назва')
    sub_cat = models.ForeignKey(SubCategoryCourse, on_delete=models.CASCADE, related_name='type_int',
                                default=None, null=True, verbose_name='Тип интелекту')
    content = models.TextField()
    currency = models.IntegerField(default=0, verbose_name='Внутршньо сервісна валюта')

    def __str__(self):
        return self.name


class ParentTasksCourseRelation(models.Model):
    class Meta:
        verbose_name_plural = 'Додання завдань до курсу'

    course_id = models.ForeignKey(ParentCourse, on_delete=models.CASCADE, related_name='started_tasks',
                                  verbose_name='Курс', null=True, default=None,)
    task_id = models.ForeignKey(ParentTask, on_delete=models.CASCADE, related_name='course_relations',
                                verbose_name='Завдання', null=True, default=None,)

    def __str__(self):
        return self.task_id.name


class ParentTaskProgress(models.Model):
    class Meta:
        verbose_name_plural = 'Прогресс проходження завдання'

    statuses = (
        ('in_progress', 'In progress'),
        ('done', 'Done')
    )
    parent_task_relation = models.ForeignKey(ParentTasksCourseRelation, on_delete=models.CASCADE,
                                             related_name='progress', verbose_name='Завдання', default=None, null=True)
    created_ad = models.DateTimeField(null=True, auto_now_add=True, verbose_name='Час початку проходження завдання')
    finished_ad = models.DateTimeField(null=True, default=None, verbose_name='Час завершення проходження завдання')
    task_status = models.CharField(max_length=50, choices=statuses, default=statuses[0][0],
                                   verbose_name='Статус проходження')
    parent = models.ForeignKey(User, on_delete=models.CASCADE, related_name='parent_task_progress_rels',
                               verbose_name='Юзер')
    child = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE, related_name='parent_test_children_rels',
                              default=None, null=True, verbose_name='Дитина')

    def __str__(self):
        return self.parent_task_relation.task_id.name

    def change_status_parent_task(self, status: str = statuses[1][0]) -> None:
        self.task_status = status
        self.save(update_fields=['task_status'])


class ParenTestScore(models.Model):
    class Meta:
        verbose_name_plural = 'Оцінка типу категорії'

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='parent_task', verbose_name='Кліент')
    sub_category_name = models.ForeignKey(SubCategoryCourse, on_delete=models.CASCADE, related_name='subcategory_task',
                                          verbose_name='Тип интелекту')
    methodologies_name = models.ForeignKey(MethodologiesCategory, on_delete=models.CASCADE,
                                           related_name='methodologies_task', verbose_name='Методологія')
    score = models.FloatField(default=0, verbose_name='Рахунок', null=True)
    child = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE, related_name='parent_test_children',
                              default=None, null=True, verbose_name='Дитина')


class ParentScoreSubCategory(models.Model):
    class Meta:
        verbose_name_plural = 'Батьківська оцінка типу інтелекту'

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='parent_score', verbose_name='Кліент')
    sub_category_name = models.ForeignKey(SubCategoryCourse, on_delete=models.CASCADE,
                                          related_name='subcategory_task_score', verbose_name='Тип интелекту')
    methodologies_name = models.ForeignKey(MethodologiesCategory, on_delete=models.CASCADE,
                                           related_name='methodologies_task_score', verbose_name='Методологія')
    score = models.FloatField(default=0, verbose_name='Рахунок', null=True)
    child = models.ForeignKey(ChildrenProfile, on_delete=models.CASCADE, related_name='parent_test_children_score',
                              default=None, null=True, verbose_name='Дитина')

