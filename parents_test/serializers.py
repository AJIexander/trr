from rest_framework import serializers


class ParentTaskStartSerializer(serializers.Serializer):
    task_id = serializers.IntegerField(required=True)
    children_id = serializers.IntegerField(required=True)


class ParentTaskFinishSerializer(serializers.Serializer):
    task_id = serializers.IntegerField(required=True)
    score = serializers.FloatField(required=True)
    children_id = serializers.IntegerField(required=True)
    currency = serializers.IntegerField(required=True)


class ParentTaskGetScoreTestsSerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)


class ParentScoreSubCategorySerializer(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)
    sub_category = serializers.IntegerField(required=True)
    methodology = serializers.IntegerField(required=True)
    score = serializers.FloatField(required=True)


class ParentResetProgress(serializers.Serializer):
    children_id = serializers.IntegerField(required=True)