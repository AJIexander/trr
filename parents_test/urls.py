from django.urls import path
from . import views

urlpatterns = [
    path('start_task/', views.ParentStartTask.as_view(), name='start_parent_task'),
    path('finish_task/', views.ParentFinishTask.as_view(), name='finish_parent_task'),
    path('get_all_task/', views.GetAllParentTask.as_view(), name='get_all_task'),
    path('get_parent_task_score/<int:children_id>/', views.GetTestParentScore.as_view(), name='get_parent_task_score'),
    path('set_parent_score/', views.SetParentScore.as_view(), name='set_parent_score'),
    path('get_parent_score/<int:children_id>/', views.GetScoreParent.as_view(), name='get_parent_score'),
    path('progress_reset_paren_test/', views.ProgressResetParenTestScore.as_view(), name='progress_reset_paren_test'),
    path('progress_reset_parent_score/', views.ProgressResetParentScoreSubCategory.as_view(),
         name='progress_reset_parent_score'),
]