import datetime
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from accounts.models import User, CurrencyWallet
from parents_test.models import ParentTaskProgress, ParenTestScore, ParentTasksCourseRelation, ParentScoreSubCategory
from parents_test.serializers import ParentTaskStartSerializer, ParentTaskFinishSerializer, \
    ParentTaskGetScoreTestsSerializer, ParentScoreSubCategorySerializer, ParentResetProgress


class ParentStartTask(APIView):
    """
    Adding and passing tests for the parent account originally endpoint displays the status of the test in "in_progress"
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ParentTaskStartSerializer)
    def post(self, request):
        serializer = ParentTaskStartSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(id=request.user.pk, is_children=None).first()
        if not user:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Test for parent account only'}}, status=401)

        task = ParentTasksCourseRelation.objects.filter(task_id=serializer.data['task_id']).first()
        progress_task = ParentTaskProgress.objects.create(parent=user,
                                                          parent_task_relation=task,
                                                          child_id=serializer.data['children_id'])
        data = {
            'task_id': progress_task.parent_task_relation.task_id.id,
            'task_name': progress_task.parent_task_relation.task_id.name,
            'task_content': progress_task.parent_task_relation.task_id.content,
            'task_methodologies': progress_task.parent_task_relation.course_id.method.name,
            'task_subcategory': progress_task.parent_task_relation.task_id.sub_cat.name,
            'task_currency': progress_task.parent_task_relation.task_id.currency,
        }
        return Response({
            'status': 'Success',
            'code': 201,
            'data': data
        }, status=201)


class ParentFinishTask(APIView):
    """
    Moving the test to "done" status
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ParentTaskFinishSerializer)
    def post(self, request):
        serializer = ParentTaskFinishSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(id=request.user.pk, is_children=None).first()
        if not user:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Test for parent account only'}}, status=401)

        wallet = CurrencyWallet.objects.filter(user=user).first()
        retake = ParentTaskProgress.objects.filter(parent=user,
                                                     parent_task_relation__task_id_id=serializer.data[
                                                         'task_id'],
                                                     task_status='done',
                                                     child_id=serializer.validated_data['children_id']).first()
        if retake is None:
            if wallet:
                wallet.currency += serializer.validated_data['currency']
            else:
                wallet = CurrencyWallet.objects.create(user=user,
                                                       currency=serializer.validated_data['currency'])
            wallet.save()
        progress = ParentTaskProgress.objects.filter(parent=user,
                                                     parent_task_relation__task_id_id=serializer.data[
                                                         'task_id'],
                                                     task_status='in_progress',
                                                     child_id=serializer.validated_data['children_id']).first()
        progress.finished_ad = datetime.datetime.now()
        progress.change_status_parent_task(status='done')
        score_test = ParenTestScore.objects.filter(child_id=serializer.validated_data['children_id'],
                                                   user=user,
                                                   sub_category_name=progress.parent_task_relation.task_id.
                                                   sub_cat,
                                                   methodologies_name=progress.parent_task_relation.course_id.
                                                   method,
                                                   ).first()
        if score_test:
            score_test.score = serializer.validated_data['score']
            score_test.save()
        else:
            score_test = ParenTestScore.objects.create(user=user,
                                                       sub_category_name=progress.parent_task_relation.task_id.
                                                       sub_cat,
                                                       methodologies_name=progress.parent_task_relation.
                                                       course_id.method,
                                                       child_id=serializer.validated_data['children_id'],
                                                       score=serializer.validated_data['score'])

        score_test.save()
        progress.save()
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': f'You currency wallet balance {wallet.currency}'}}, status=201)


class GetAllParentTask(APIView):
    """
    Get all task for parent account
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request):
        user = User.objects.filter(id=request.user.pk, is_children=None).first()
        if not user:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Test for parent account only'}}, status=401)

        queryset = ParentTasksCourseRelation.objects.all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'Not assign parent task'}}, status=404)

        curses = {}
        for rel in queryset:
            if rel.course_id.name not in curses.keys():
                curses[rel.course_id.name] = rel.course_id
        data = [{'curs_id': curs.id,
                 'curs_name': curs.name,
                 'methodologies_id': curs.method.id,
                 'methodologies_name': curs.method.name,
                 'sub_category_id': curs.get_sub_id(),
                 'sub_category_name': curs.get_sub_cat_tasks(),
                 'tasks': {'task_id': curs.get_all_parent_tasks_id(),
                           'task_name': curs.get_all_parent_tasks_names()}}
                for curs in curses.values()]
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class GetTestParentScore(APIView):
    """
    Getting a parent's test bill using children_id
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = ParentTaskGetScoreTestsSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = ParenTestScore.objects.filter(child=serializer.validated_data['children_id']).all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': 'No tests passed'}}, status=401)

        data = []
        for query in queryset:
            data.append({
                'methodologies_id': query.methodologies_name.id,
                'sub_category_id': query.sub_category_name.id,
                'methodologies_name': query.methodologies_name.name,
                'sub_category_name': query.sub_category_name.name,
                'score': query.score,
                'child': query.child.children_user.username
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class SetParentScore(APIView):
    """

    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ParentScoreSubCategorySerializer)
    def post(self, request):
        serializer = ParentScoreSubCategorySerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        user = User.objects.filter(id=request.user.pk, is_children=None).first()
        if not user:
            return Response({
                'status': 'Error',
                'code': 401,
                'data': {'description': 'Test for parent account only'}}, status=401)

        retake = ParentScoreSubCategory.objects.filter(user=user,
                                                             sub_category_name_id=serializer.validated_data[
                                                                 'sub_category'],
                                                             methodologies_name_id=serializer.validated_data[
                                                                 'methodology'],
                                                             child_id=serializer.validated_data['children_id']
                                                             ).first()
        if retake:
            retake.score = serializer.validated_data['score']
            retake.save()
        else:
            ParentScoreSubCategory.objects.create(user=user,
                                                             sub_category_name_id=serializer.validated_data[
                                                                 'sub_category'],
                                                             methodologies_name_id=serializer.validated_data[
                                                                 'methodology'],
                                                             score=serializer.validated_data['score'],
                                                             child_id=serializer.validated_data['children_id']
                                                             )
        return Response({
            'status': 'Success',
            'code': 201,
            'data': {'description': 'Your rating is saved'}}, status=201)


class GetScoreParent(APIView):
    """
    Receiving points based on the type of intelligence contributed by the client
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema()
    def get(self, request, children_id: int):
        serializer = ParentTaskGetScoreTestsSerializer(data={'children_id': children_id})
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        queryset = ParentScoreSubCategory.objects.filter(child_id=serializer.validated_data['children_id']).all()
        if not queryset:
            return Response({
                'status': 'Error',
                'code': 404,
                'data': {'description': "You haven't added a rating yea"}}, status=404)

        data = []
        for query in queryset:
            data.append({
                'methodologies_id': query.methodologies_name.id,
                'sub_category_id': query.sub_category_name.id,
                'methodologies_name': query.methodologies_name.name,
                'sub_category_name': query.sub_category_name.name,
                'score': query.score,
                'child': query.child.children_user.username
            })
        return Response({
            'status': 'Success',
            'code': 200,
            'data': data
        }, status=200)


class ProgressResetParenTestScore(APIView):
    """
    Resetting the parent's progress
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ParentResetProgress)
    def post(self, request):
        serializer = ParentResetProgress(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        ParenTestScore.objects.filter(child=serializer.validated_data['children_id']).delete()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)


class ProgressResetParentScoreSubCategory(APIView):
    """
    Resetting the parent's progress
    """
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(request_body=ParentResetProgress)
    def post(self, request):
        serializer = ParentResetProgress(data=request.data)
        if not serializer.is_valid():
            return Response({
                'status': 'Error',
                'code': 400,
                'data': serializer.errors
            }, status=400)

        ParentScoreSubCategory.objects.filter(child=serializer.validated_data['children_id']).delete()
        return Response({
            'status': 'Success',
            'code': 200,
            'data': {
                'description': 'Done!'
            }
        }, status=200)
