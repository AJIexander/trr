import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'email, username, age, gender, password, status_code', [
        ('children@example.com', 'username', 10, 'male', 'slim199700', 201),
        ('', '', '', '', '', 400),
        ('', 'username', 10, 'male', 'slim199700', 400),
        ('children@example.com', '', 10, 'male', 'slim199700', 400),
        ('children@example.com', 'username', 'str', 'male', 'slim199700', 400),
        ('children@example.com', 'username', 10, 'male', '', 400),
    ]
)
@pytest.mark.django_db
def test_add_children(email, username, age, gender, password, status_code, client, auth):
    url = reverse('add_child')
    data = {
        'email': email,
        'username': username,
        'age': age,
        'gender': gender,
        'password': password
    }

    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
