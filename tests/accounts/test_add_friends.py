import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_add_friend_true(auth, client, create_user, create_two_user):
    url = reverse('add_friends')
    data = {
        'client_id': create_two_user.id,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.django_db
def test_add_friend_false(auth, client, create_user):
    url = reverse('add_friends')
    data = {
        'client_id': 200
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 404