import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_change_parent_for_children_true(create_children, create_user, create_two_user, client, auth):
    url = reverse('change_parent')
    data = {
        'user_id': create_two_user.id,
        'children_id': create_children.id,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.django_db
def test_change_parent_for_children_false(create_user, create_two_user, client, auth):
    url = reverse('change_parent')
    data = {
        'user_id': create_two_user.id,
        'children_id': 100,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 400