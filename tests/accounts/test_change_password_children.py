import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'old_password, new_password, status_code', [
        ('slim199700', 'makeleader199700', 201),
        ('', 'makeleader199700', 400),
        ('slim199700', '', 400),
    ]
)
@pytest.mark.django_db
def test_change_password_children(old_password, new_password, status_code, create_children_user, create_children,
                                  client, auth, auth_children):
    url = reverse('change_children_password')
    data = {
        'old_password': old_password,
        'new_password': new_password
    }
    response = client.post(url, data=data, **auth_children)
    assert response.status_code == status_code


@pytest.mark.parametrize(
    'old_password, new_password, status_code', [
        ('slim199700', 'makeleader199700', 201),
        ('', 'makeleader199700', 400),
        ('slim199700', '', 400),
    ]
)
@pytest.mark.django_db
def test_change_password_children_account_parent(old_password, new_password, status_code, create_children_user,
                                                 create_children,
                                                 client, auth):
    url = reverse('change_children_password')
    data = {
        'id': create_children_user.id,
        'old_password': old_password,
        'new_password': new_password
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
