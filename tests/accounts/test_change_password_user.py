import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'old_password, new_password, status_code', [
        ('slim199700', 'makeleader199700', 201),
        ('', 'makeleader199700', 400),
        ('slim199700', '', 400),
    ]
)
@pytest.mark.django_db
def test_change_password(old_password, new_password, status_code, create_user, client, auth):
    url = reverse('change_password_user')
    data = {
        'old_password': old_password,
        'new_password': new_password
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code