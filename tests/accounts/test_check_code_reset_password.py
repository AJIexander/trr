import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_check_code_reset_password_true(create_user, client, create_code_reset_password):
    url = reverse('check_code')
    data = {
        'code': create_code_reset_password.code,
        'new_password': 'testpassword1234567890'
    }
    response = client.post(url, data=data)
    assert response.status_code == 201


@pytest.mark.parametrize('code, new_password, status_code', [
    ('1', 'testpassword1234567890', 404),
    ('1111', '', 400)
])
@pytest.mark.django_db
def test_check_code_reset_password_false(client, code, new_password, status_code):
    url = reverse('check_code')
    data = {
        'code': code,
        'new_password': new_password
    }
    response = client.post(url, data=data)
    assert response.status_code == status_code