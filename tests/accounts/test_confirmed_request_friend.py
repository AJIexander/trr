import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_confirmed_request_friend_true(client, auth, create_confirmed_request, create_two_user):
    url = reverse('confirmed_request_friend')
    data = {
        'client_id': create_two_user.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_confirmed_request_friend_false(client, auth):
    url = reverse('confirmed_request_friend')
    data = {
        'client_id': 200
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 404