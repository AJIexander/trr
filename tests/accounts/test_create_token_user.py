import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'email, password, status_code', [
        ('user@example.com', 'slim199700', 200),
        ('None', 'None', 401),
        ('None', 'slim199700', 401),
        ('user@example.com', 'None', 401),
    ]
)
@pytest.mark.django_db
def test_create_token(api_client, email, password, status_code):
    url = reverse('login')
    data = {
        'email': email,
        'password': password
    }
    response = api_client.post(url, data=data)
    assert response.status_code == status_code
