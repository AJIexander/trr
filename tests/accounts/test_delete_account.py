import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_delete_account(create_user, client, auth):
    url = reverse('delete_account')
    response = client.delete(url, **auth)
    assert response.status_code == 200
