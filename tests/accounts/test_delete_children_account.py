import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_delete_children_account_balance_wallet_not_null_true(create_user,
                                                         create_children_wallet, create_children, client, auth):
    url = reverse('delete_children_account', args=([create_children.id]))
    response = client.delete(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_delete_children_account_true(create_user, create_children_wallet_null, create_children, client, auth):
    url = reverse('delete_children_account', args=([create_children.id]))
    response = client.delete(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_delete_children_false(create_user, client, auth):
    url = reverse('delete_children_account', args=([13]))
    response = client.delete(url, **auth)
    assert response.status_code == 404