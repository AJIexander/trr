import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_delete_friends_true(auth, client, create_user, create_two_user):
    url = reverse('delete_friends', args=[create_two_user.id])
    response = client.delete(url, **auth)
    assert response.status_code == 200
