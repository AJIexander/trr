import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'username, age, gender, status_code', [
        ('username', 10, 'male', 200),
        ('', 10, 'male', 200),
        ('username', str, 'male', 400),
        ('username', 10, '', 200),
    ]
)
@pytest.mark.django_db
def test_edit_children_profile(username, age, gender, status_code, client, auth, create_children):
    url = reverse('edit_children_profile')
    data = {
        'id': create_children.id,
        'username': username,
        'age': age,
        'gender': gender,
    }

    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
