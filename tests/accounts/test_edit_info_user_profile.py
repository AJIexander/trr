import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'username, age, country, city, zip_code, phone_number, status_code', [
        ('dev', 25, 'Ukraine', 'Kiyv', 39600, '380686404773', 200),
    ]
)
@pytest.mark.django_db
def test_edit_info_user_profile(username, age, country, city, zip_code, phone_number, status_code, client, auth,
                                create_user_profile):
    url = reverse('edit_info_user_profile')
    data = {
        'username': username,
        'age': age,
        'country': country,
        'city': city,
        'zip_code': zip_code,
        'phone_number': phone_number
    }

    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code