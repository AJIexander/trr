import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_friend_confirmed_children_true(client, auth, create_user, create_two_user, create_friend):
    url = reverse('friend_confirmed_children')
    data = {
        'client_id': create_two_user.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_friend_confirmed_children_false(client, auth, create_user, create_two_user):
    url = reverse('friend_confirmed_children')
    data = {
        'client_id': create_two_user.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 404