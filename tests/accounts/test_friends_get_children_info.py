import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_friends_get_children_info_true(client, auth_two, create_two_user, create_user,
                                        create_confirmed_request_true, create_children):
    url = reverse('friends_get_children_info', args=([create_user.id]))
    response = client.get(url, **auth_two)
    assert response.status_code == 200


@pytest.mark.django_db
def test_friends_get_children_info_false(client, auth):
    url = reverse('friends_get_children_info', args=([200]))
    response = client.get(url, **auth)
    assert response.status_code == 401