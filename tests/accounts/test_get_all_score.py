import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_score(client, auth, create_children):
    url = reverse('get_all_score')
    response = client.get(url, **auth)
    assert response.status_code == 200
    response = client.get(url)
    assert response.status_code == 401
