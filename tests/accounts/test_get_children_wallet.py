import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_children_wallet_true(client, auth, create_user, create_wallet, create_children):
    url = reverse('get_children_wallet', args=([create_children.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_children_wallet_false(client):
    url = reverse('get_children_wallet', args=([240]))
    response = client.get(url)
    assert response.status_code == 401