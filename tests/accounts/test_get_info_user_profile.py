import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_info_user_profile_true(client, auth, create_user_profile, create_user):
    url = reverse('get_info_user_profile', args=([create_user.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_info_user_profile_fale(client, auth, create_user_profile):
    url = reverse('get_info_user_profile', args=([203]))
    response = client.get(url, **auth)
    assert response.status_code == 404
