import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_my_friends_true(client, auth, create_friend):
    url = reverse('get_my_friends')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_my_friends_false(client, auth):
    url = reverse('get_my_friends')
    response = client.get(url, **auth)
    assert response.status_code == 404