import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_my_profile_info(client, auth, create_user_profile, create_user):
    url = reverse('get_my_profile_info')
    response = client.get(url, **auth)
    assert response.status_code == 200
