import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_my_wallet_true(client, auth, create_user, create_wallet):
    url = reverse('get_my_wallet')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_my_wallet_false(client):
    url = reverse('get_my_wallet')
    response = client.get(url)
    assert response.status_code == 401