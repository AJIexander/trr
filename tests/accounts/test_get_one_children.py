import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_one_children_true(client, auth, create_children):
    url = reverse('get_one_children', args=([create_children.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_one_children_false(client, auth, create_children):
    url = reverse('get_one_children', args=([100]))
    response = client.get(url, **auth)
    assert response.status_code == 400
