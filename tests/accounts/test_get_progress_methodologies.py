import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_progress_methodologies_true(client, auth, create_user, create_children, create_course):
    url = reverse('get_progress_methodologies', args=([create_children.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200
