import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_request_friend_true(client, auth, create_confirmed_request):
    url = reverse('get_request_friend')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_request_friend_false(client, auth):
    url = reverse('get_request_friend')
    response = client.get(url, **auth)
    assert response.status_code == 404