import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_user_true(client, auth, create_user):
    url = reverse('get_users', args=([create_user.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_user_false(client, auth):
    url = reverse('get_users', args=([404]))
    response = client.get(url, **auth)
    assert response.status_code == 404