import pytest
from django.urls import reverse


@pytest.mark.parametrize(
    'email, username, password, phone_number, status_code', [
        ('user@example.com', 'test_name', 'strong_pass', '1234567890', 201),
        ('None', 'None', 'None', 'None', 400),
        ('None', 'None', 'strong_pass', '1234567890', 400),
        ('user@example.com', 'None', 'None', '1234567890', 400),
        ('user@example.com', 'test_name', 'None', '1234567890', 400),
        ('user@example.com', 'test_name', 'strong_pass', '', 400),
    ]
)
@pytest.mark.django_db
def test_register_user(email, username, password, phone_number, status_code, client):
    url = reverse('register')
    data = {
        'email': email,
        'username': username,
        'password': password,
        'phone_number': phone_number,
    }
    response = client.post(url, data=data)
    assert response.status_code == status_code