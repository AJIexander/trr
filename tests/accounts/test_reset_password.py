import pytest
from django.urls import reverse


@pytest.mark.parametrize('email, status_code', [
    ('user@example.com', 201),
    ('', 400)
])


@pytest.mark.django_db
def test_reset_password(create_user, client, email, status_code):
    url = reverse('reset_password')
    data = {
        'email': email,
    }
    response = client.post(url, data=data)
    assert response.status_code == status_code