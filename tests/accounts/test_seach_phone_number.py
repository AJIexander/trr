import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_search_user_true(client, auth, create_user):
    url = reverse('search_user', args=([create_user.phone_number]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_search_user_false(client, auth):
    url = reverse('search_user', args=(['fake_number']))
    response = client.get(url, **auth)
    assert response.status_code == 404