import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_transfer_currency_true(create_wallet, create_children, client, auth):
    url = reverse('transfer_currency')
    data = {
        'currency': 50,
        'user_id': create_children.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.django_db
def test_transfer_currency_false(create_wallet, create_children, client, auth):
    url = reverse('transfer_currency')
    data = {
        'currency': 5000,
        'user_id': create_children.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 400