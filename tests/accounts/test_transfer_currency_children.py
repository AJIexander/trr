
import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_transfer_currency_children_true(create_user, create_children_wallet, create_children_user, client, auth):
    url = reverse('transfer_currency_children')
    data = {
        'currency': 50,
        'user_id': create_children_user.id,
        'recipient_id': create_user.id,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.django_db
def test_transfer_currency_children_false(create_children_wallet, create_user, create_children_user, client, auth):
    url = reverse('transfer_currency_children')
    data = {
        'currency': 5000,
        'user_id': create_children_user.id,
        'recipient_id': create_user.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 400