import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_one_category(client, create_age_category, auth):
    url = reverse('get_category', args=([create_age_category.age]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_one_category_false(client, auth):
    url = reverse('get_category', args=([5]))
    response = client.get(url, **auth)
    assert response.status_code == 404