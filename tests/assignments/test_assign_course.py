import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_assign_course_true(create_children, create_course, client, auth):
    url = reverse('assign_course')
    data = {
        'children_id': create_children.id,
        'course_id': create_course.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'children_id, course_id, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_assign_course_false(children_id, course_id, status_code, client, auth):
    url = reverse('assign_course')
    data = {
        'children_id': children_id,
        'course_id': course_id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
