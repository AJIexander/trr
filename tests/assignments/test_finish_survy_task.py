import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_finish_survey_task_true(create_user_survey_task, client, auth, create_course, create_children, create_user):
    url = reverse('finish_survey_task')
    data = {
        'children_id': create_children.id,
        'survey_course_id': create_user_survey_task.id,
        'comment': 'comment',
        'score': 100,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'children_id, survey_course_id, comment, score, status_code', [
        ('', '', '', '', 400),
        ('', 1, 'test', 100, 400),
        (1, '', 'test', 100, 400),
        (1, 1, '', 100, 400),
        (1, 1, 'test', '', 400),
    ]
)
@pytest.mark.django_db
def test_finish_survey_task_false(client, auth, children_id, survey_course_id, comment, score, status_code):
    url = reverse('finish_survey_task')
    data = {
        'children_id': children_id,
        'survey_course_id': survey_course_id,
        'comment': comment,
        'score': score,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
