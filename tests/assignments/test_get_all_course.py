import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_category(create_course, client, auth, create_task_course_relation, create_children):
    url = reverse('get_course', args=([create_children.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_category_false(client, auth):
    url = reverse('get_course', args=([1]))
    response = client.get(url, **auth)
    assert response.status_code == 404
