import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_sub_category(create_sub_category, client, auth):
    url = reverse('get_all_sub_cat')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_sub_category_false(client, auth):
    url = reverse('get_all_sub_cat')
    response = client.get(url, **auth)
    assert response.status_code == 404
