import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_progress_true(create_task, create_course, client, auth_children, create_children, status_progress,
                          course_children_relation):
    url = reverse('get_progress', args=([create_children.id]))

    response = client.get(url, **auth_children)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_progress_false(client):
    url = reverse('get_progress', args=([7]))
    response = client.get(url)
    assert response.status_code == 401