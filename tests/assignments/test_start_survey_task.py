import pytest
from django.urls import reverse

@pytest.mark.django_db
def test_start_survey_task_true(create_user_survey_task, client, auth, create_course):
    url = reverse('start_survey_task')
    data = {
        'course_id': create_course.id,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_start_survey_task_false(client, auth):
    url = reverse('start_survey_task')
    data = {
        'course_id': 25,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 404
