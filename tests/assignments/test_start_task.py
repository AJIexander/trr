import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_start_task_true(create_task, create_course, client, auth_children, create_children, create_progress):
    url = reverse('start_task')
    data = {
        'task_id': create_task.id,
        'course_id': create_course.id
    }
    response = client.post(url, data=data, **auth_children)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'task_id, course_id, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_start_task_false(task_id, course_id, status_code, client, auth_children, create_children, create_progress):
    url = reverse('start_task')
    data = {
        'task_id': task_id,
        'course_id': course_id
    }
    response = client.post(url, data=data, **auth_children)
    assert response.status_code == status_code
