import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_favorite_state_true(client, create_state, auth, create_favorite_state):
    url = reverse('delete_favorite_state')
    data = {
        'state_id': create_favorite_state.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_favorite_state_false(client, create_state, auth):
    url = reverse('delete_favorite_state')
    data = {
        'state_id': 'str'
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 400