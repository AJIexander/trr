import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_multi_true(client, create_state, create_multi_category_relations, auth):
    url = reverse('get_all_multi')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_multi_false(client, auth):
    url = reverse('get_all_multi')
    response = client.get(url, **auth)
    assert response.status_code == 404