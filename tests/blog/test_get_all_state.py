import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_state_true(client, create_state, auth):
    url = reverse('get_all_state')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_state_false(client, auth):
    url = reverse('get_all_state')
    response = client.get(url, **auth)
    assert response.status_code == 404
