import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_favorite_state_true(client, create_state, auth, create_favorite_state):
    url = reverse('get_favorite_state')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_favorite_state_false(client, create_state, auth):
    url = reverse('get_favorite_state')
    response = client.get(url, **auth)
    assert response.status_code == 404