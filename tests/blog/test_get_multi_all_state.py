import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_multi_all_state_true(client, create_state, create_multi_category_relations, auth):
    url = reverse('get_multi_all_state', args=([create_multi_category_relations.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_multi_all_state_false(client, auth):
    url = reverse('get_multi_all_state', args=([1]))
    response = client.get(url, **auth)
    assert response.status_code == 404
