import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_state_one_true(client, create_state, auth):
    url = reverse('get_state', args=([create_state.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_state_one_false(client, auth):
    url = reverse('get_state', args=([1]))
    response = client.get(url, **auth)
    assert response.status_code == 404
