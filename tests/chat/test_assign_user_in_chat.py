import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_assign_members_true(create_chat, create_children_user, client, auth):
    url = reverse('assign_user_in_chat')
    data = {
        'chat_id': create_chat.id,
        'members': create_children_user.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.parametrize(
    'chat_id, members, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_assign_members_false(chat_id, members, status_code, client, auth):
    url = reverse('assign_user_in_chat')
    data = {
        'chat_id': chat_id,
        'members': members
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code