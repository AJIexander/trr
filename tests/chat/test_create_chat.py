import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_create_chat_true(create_children, client, auth):
    url = reverse('create_chat')
    data = {
        'name': 'testName',
        'members': create_children.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'name, members, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_create_chat_false(name, members, status_code, client, auth):
    url = reverse('create_chat')
    data = {
        'name': name,
        'members': members
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code



