import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_delete_user_from_chat_true(create_chat, add_members_in_chat, client, auth):
    url = reverse('delete_user_from_chat')
    data = {
        'chat_id': create_chat.id,
        'members_id': add_members_in_chat.id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.parametrize(
    'chat_id, members_id, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_delete_user_from_chat_false(chat_id, members_id, status_code, client, auth):
    url = reverse('delete_user_from_chat')
    data = {
        'chat_id': chat_id,
        'members_id': members_id
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code




