import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_edit_message_true(create_chat, create_message_in_chat, client, auth):
    url = reverse('edit_message')
    data = {
        'chat_id': create_chat.id,
        'message_id': create_message_in_chat.id,
        'message': 'str'
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 200


@pytest.mark.parametrize(
    'chat_id, message_id, status_code', [
        ('', '', 400),
        ('', 1, 400),
        (1, '', 400),
    ]
)
@pytest.mark.django_db
def test_edit_message_false(chat_id, message_id, status_code, client, auth):
    url = reverse('edit_message')
    data = {
        'chat_id': chat_id,
        'message_id': message_id,
        'message': 'str',
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code




