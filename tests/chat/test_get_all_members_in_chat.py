import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_members_in_chat_true(create_chat, add_members_in_chat, client, auth):
    url = reverse('get_all_members_in_chat', args=([create_chat.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_members_in_chat_false(client, auth):
    url = reverse('get_all_members_in_chat', args=([100]))
    response = client.get(url, **auth)
    assert response.status_code == 400