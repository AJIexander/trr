import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_message_true(create_message_in_chat, client, auth):
    url = reverse('get_all_message', args=([create_message_in_chat.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_message_false(client, auth):
    url = reverse('get_all_message', args=([100]))
    response = client.get(url, **auth)
    assert response.status_code == 400

