import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_member(client, auth, create_children):
    url = reverse('get_members')
    response = client.get(url, **auth)
    assert response.status_code == 200
    url = reverse('get_members')
    response = client.get(url)
    assert response.status_code == 401