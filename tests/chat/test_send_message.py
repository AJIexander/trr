import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_send_message_true(create_chat, client, auth):
    url = reverse('send_message')
    data = {
        'chat_id': create_chat.id,
        'message': 'str'
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'chat_id, message, status_code', [
        ('', '', 400),
        ('', 1, 400),
    ]
)
@pytest.mark.django_db
def test_send_message_false(chat_id, message, status_code, client, auth):
    url = reverse('send_message')
    data = {
        'chat_id': chat_id,
        'message': message
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code