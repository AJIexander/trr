import secrets

from accounts.models import User, ChildrenProfile, AllScore, ResetPasswordCode, UserProfile, CurrencyWallet, Friends
from age_category.models import AgeCategoryRelations, AgeCategory
from assignments.models import Course, Task, TaskProgress, TasksCourseRelation, ChildrenCourseRelation, UserSurveyTask, \
    SubCategoryCourse, MethodologiesCategory
from blog.models import MultiCategory, Category, MultiCategoryRelation, State, ArticleReview
from chat.models import Chat, ChatMember, ChatMessage
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

import pytest

from parents_test.models import ParentTask, ParentTaskProgress, ParentCourse, ParentTasksCourseRelation, ParenTestScore, \
    ParentScoreSubCategory


@pytest.fixture
def create_user():
    user = User.objects.create_user(username='slimireiner', email='user@example.com', password='slim199700',
                                    phone_number='1234567890')
    return user


@pytest.fixture
def create_two_user():
    two_user = User.objects.create_user(username='two_user', email='twouser@example.com', password='slim199700',
                                        phone_number='1234567891')
    return two_user


@pytest.fixture
def create_children_user(create_user):
    children_user = User.objects.create_user(username='username', email='children@example.com', password='slim199700',
                                             is_children=True, parent_id=create_user.id)
    return children_user


@pytest.fixture
def api_client(create_user):
    client = APIClient()
    refresh = RefreshToken.for_user(create_user)
    client.credentials(HTTP_AUTHORIZATION=f'Bearer {refresh.access_token}')
    return client


@pytest.fixture
def auth(create_user):
    refresh = RefreshToken.for_user(create_user)
    return {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}


@pytest.fixture
def auth_two(create_two_user):
    refresh = RefreshToken.for_user(create_two_user)
    return {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}



@pytest.fixture
def create_children(create_children_user):
    children_profile = ChildrenProfile.objects.create(children_user=create_children_user, age=10, gender='male')
    AllScore.objects.create(kid=children_profile)
    return children_profile


@pytest.fixture
def auth_children(create_children_user):
    refresh = RefreshToken.for_user(create_children_user)
    return {'HTTP_AUTHORIZATION': f'Bearer {refresh.access_token}'}


@pytest.fixture
def create_methodologies():
    methodologies = MethodologiesCategory.objects.create(name="Методология (Гарднер)", content='testContent')
    return methodologies


@pytest.fixture
def create_sub_category(create_methodologies):
    sub_catery = SubCategoryCourse.objects.create(name='Логикоматематический интеллект', content='testContent',
                                                  methodologies=create_methodologies)
    return sub_catery


@pytest.fixture
def create_course(create_sub_category):
    course = Course.objects.create(name='testCourse', sub_category=create_sub_category)
    return course


@pytest.fixture
def create_task():
    task = Task.objects.create(
        name='testTask',
        score=10,
        content='testContent',
        task_image_one='1',
        task_image_two='2',
        task_image_three='3',
        task_image_four='4',
        task_image_five='5',
        task_image_true='4',
    )
    return task


@pytest.fixture
def create_task_course_relation(create_course, create_task):
    relation = TasksCourseRelation.objects.create(course_id=create_course, task_id=create_task)
    return relation


@pytest.fixture
def create_progress(create_children, create_task, create_task_course_relation):
    progress = TaskProgress.objects.create(children=create_children, task_relation=create_task_course_relation)
    return progress


@pytest.fixture
def status_progress(create_children, create_task, create_task_course_relation):
    progress = TaskProgress.objects.create(children=create_children, task_relation=create_task_course_relation,
                                           task_status='in_progress')
    return progress


@pytest.fixture
def course_children_relation(create_children, create_course):
    relations = ChildrenCourseRelation.objects.create(children=create_children, course=create_course)
    return relations


@pytest.fixture
def create_chat(create_user, create_children_user):
    chat = Chat.objects.create(name='testChat')
    members = [ChatMember(user=create_user, is_owner=True, is_moderator=True, chat=chat)] + [
        ChatMember(user=create_children_user, chat=chat, is_moderator=True)
    ]
    ChatMember.objects.bulk_create(members, ignore_conflicts=True)
    return chat


@pytest.fixture
def add_members_in_chat(create_children_user, create_chat):
    members = ChatMember.objects.create(user=create_children_user, chat=create_chat)
    return members


@pytest.fixture
def create_message_in_chat(create_chat, create_user):
    message = ChatMessage.objects.create(sender=create_user, chat=create_chat, message='str')
    return message


@pytest.fixture
def create_multi_category():
    multi = MultiCategory.objects.create(name='Test', description='test')
    return multi


@pytest.fixture
def create_category():
    category = Category.objects.create(name='Test', description='test')
    return category


@pytest.fixture
def create_multi_category_relations(create_multi_category, create_category):
    relation = MultiCategoryRelation.objects.create(multi_category=create_multi_category)
    return relation


@pytest.fixture
def create_state(create_multi_category_relations, create_user, create_category):
    state = State.objects.create(title='test', content='test', multi_category=create_multi_category_relations,
                                 category=create_category, author=create_user)
    return state


@pytest.fixture
def create_category_relations(create_user, create_course):
    relation = AgeCategoryRelations.objects.create(name='test')
    return relation


@pytest.fixture
def create_age_category(create_category_relations, create_user):
    age_category = AgeCategory.objects.create(age=5)
    return age_category


@pytest.fixture
def create_parent_course(create_methodologies):
    parent_course = ParentCourse.objects.create(name='parent_course', content='testContent',
                                                method=create_methodologies)
    return parent_course


@pytest.fixture
def create_parent_task(create_sub_category):
    parents_task = ParentTask.objects.create(name='test', content='testContent', sub_cat=create_sub_category)
    return parents_task


@pytest.fixture
def create_parent_rels(create_parent_course, create_parent_task):
    rels_task_course = ParentTasksCourseRelation.objects.create(course_id=create_parent_course,
                                                                task_id=create_parent_task)
    return rels_task_course


@pytest.fixture
def create_progress_parent_task(create_parent_rels, create_user, create_children):
    progress_parent_task = ParentTaskProgress.objects.create(parent_task_relation=create_parent_rels,
                                                             task_status='in_progress',
                                                             parent=create_user, child=create_children)
    return progress_parent_task


@pytest.fixture
def create_code_reset_password(create_user):
    code_reset = ResetPasswordCode.objects.create(user=create_user, code='1111')
    return code_reset


@pytest.fixture
def create_user_survey_task(create_course):
    user_survey_task = UserSurveyTask.objects.create(name='test', content='p', course=create_course)
    return user_survey_task


@pytest.fixture
def create_favorite_state(create_state, create_user):
    favorite_state = ArticleReview.objects.create(state_review=create_state, user=create_user, favorite=True)
    return favorite_state


@pytest.fixture
def create_user_profile(create_user):
    user_profile = UserProfile.objects.create(client=create_user, country='Ukraine', city='Kiyv',
                                              zip_code=98399, age=25)
    return user_profile


@pytest.fixture
def create_parent_test_score(create_user, create_children, create_sub_category, create_methodologies):
    test_score = ParenTestScore.objects.create(user=create_user, sub_category_name=create_sub_category,
                                               methodologies_name=create_methodologies, score=6, child=create_children)
    return test_score


@pytest.fixture
def create_parent_score(create_user, create_children, create_sub_category, create_methodologies):
    parent_score = ParentScoreSubCategory.objects.create(user=create_user, sub_category_name=create_sub_category,
                                                         methodologies_name=create_methodologies, score=6,
                                                         child=create_children)
    return parent_score


@pytest.fixture
def create_wallet(create_user):
    wallet = CurrencyWallet.objects.create(user=create_user, currency=100)
    return wallet


@pytest.fixture
def create_children_wallet(create_children_user):
    children_wallet = CurrencyWallet.objects.create(user=create_children_user, currency=100)
    return children_wallet


@pytest.fixture
def create_confirmed_request(create_user, create_two_user):
    add_friend = Friends.objects.create(user=create_two_user, friend=create_user, confirmed=False,
                                        user_confirmed_children=False, friend_confirmed_children=False)
    return add_friend


@pytest.fixture
def create_friend(create_user, create_two_user):
    friend = Friends.objects.create(user=create_two_user, friend=create_user, confirmed=True)
    return friend


@pytest.fixture
def create_children_wallet_null(create_children_user):
    children_wallet = CurrencyWallet.objects.create(user=create_children_user, currency=0)
    return children_wallet


@pytest.fixture
def create_confirmed_request_true(create_user, create_two_user):
    confirmed_true = Friends.objects.create(user=create_two_user, friend=create_user, confirmed=True,
                                            user_confirmed_children=True, friend_confirmed_children=True)
    return confirmed_true