import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_finish_parent_task_true(create_progress_parent_task, create_parent_task, client, auth, create_children):
    url = reverse('finish_parent_task')
    data = {
        'task_id': create_parent_task.id,
        'children_id': create_children.id,
        'score': 100,
        'currency': 100,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201


@pytest.mark.parametrize(
    'task_id, status_code', [
        ('', 400),
    ]
)
@pytest.mark.django_db
def test_finish_parent_task_false(task_id, status_code, client, auth):
    url = reverse('finish_parent_task')
    data = {
        'task_id': task_id,
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == status_code
