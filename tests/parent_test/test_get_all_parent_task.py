import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_all_parent_task_true(create_parent_rels, client, auth):
    url = reverse('get_all_task')
    response = client.get(url, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_get_all_parent_task_false(client, auth):
    url = reverse('get_all_task')
    response = client.get(url, **auth)
    assert response.status_code == 404