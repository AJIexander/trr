import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_get_parent_task_score_true(create_parent_score, client, auth, create_children):
    url = reverse('get_parent_score',  args=([create_children.id]))
    response = client.get(url, **auth)
    assert response.status_code == 200