import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_progress_reset_paren_test_true(create_parent_test_score, client, auth, create_children):
    url = reverse('progress_reset_paren_test')
    data = {
        'children_id': create_children.id,
    }
    response = client.post(url, data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_progress_reset_paren_test_false(client, auth):
    url = reverse('progress_reset_paren_test')
    data = {
        'children_id': 'q',
    }
    response = client.post(url, data, **auth)
    assert response.status_code == 400