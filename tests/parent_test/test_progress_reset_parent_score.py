import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_progress_reset_parent_score_true(create_parent_test_score, create_sub_category, client, auth, create_children):
    url = reverse('progress_reset_parent_score')
    data = {
        'sub_category_id': create_sub_category.id,
        'children_id': create_children.id,
    }
    response = client.post(url, data, **auth)
    assert response.status_code == 200


@pytest.mark.django_db
def test_progress_reset_parent_score_false(client, auth):
    url = reverse('progress_reset_parent_score')
    data = {
        'sub_category_id': 's',
        'children_id': 'q',
    }
    response = client.post(url, data, **auth)
    assert response.status_code == 400