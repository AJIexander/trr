import pytest
from django.urls import reverse


@pytest.mark.django_db
def test_set_parent_score_true(create_parent_test_score, client, auth, create_children, create_sub_category,
                                    create_methodologies):
    url = reverse('set_parent_score')
    data = {
        'children_id': create_children.id,
        'sub_category': create_sub_category.id,
        'methodology': create_methodologies.id,
        'score': 6
    }
    response = client.post(url, data=data, **auth)
    assert response.status_code == 201